FROM node:20-alpine
RUN mkdir -p /var/lib/synch_educa_plumb
COPY . ./var/lib/synch_educa_plumb/
ENV PORT 3000
EXPOSE ${PORT}
WORKDIR /var/lib/synch_educa_plumb/
RUN npm install
CMD ["node","/var/lib/synch_educa_plumb/app.js"]
