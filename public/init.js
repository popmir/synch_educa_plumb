var SynchEduca = SynchEduca || {};

function initSynchEduca() {
    SynchEduca.DEBUG = true;
    SynchEduca.scheduler = new SynchEduca.Scheduler();
    SynchEduca.viewExecutionOutput = new SynchEduca.ViewExecutionOutput();
    SynchEduca.connectionsModel = new SynchEduca.ModelDashboardConnections();
    SynchEduca.viewDashboard = new SynchEduca.ViewDashboard(SynchEduca.viewExecutionOutput);
    SynchEduca.viewCreateComponent = new SynchEduca.ViewCreateComponent(SynchEduca.viewDashboard);
    SynchEduca.viewDashboard.attachListeners();
    SynchEduca.stopwatch = new SynchEduca.ViewStopwatch();
}

function initJsPlumbStuff() {
    var j = SynchEduca.jsPlumbInstance = jsPlumb.getInstance({
        Container: "canvas",
        Connector: "StateMachine",
        Endpoint: ["Dot", {radius: 3}],
        Anchor: "Center"
    });

    //Define endpoint options for source and target with style for connecting
    // endpoint: [ "Rectangle", { width:20, height:10 } ], 
    var sourceEndpointOptions = {
        isSource: true,
        isTarget: false,
        endpoint: ["Dot", {radius: 10}],
        paintStyle: {
            stroke: "#7AB02C",
            fill: "transparent",
            radius: 7,
            strokeWidth: 2
        },
        connector: ["Bezier", {curveness: 175}],
        connectorStyle: {strokeWidth: 10, stroke: 'blue'}
    };
    var targetEndpointOptions = {
        isSource: false,
        isTarget: true,
        endpoint: ["Dot", {radius: 10}],
        paintStyle: {
            fill: "#7AB02C",
            radius: 7
        },
        connector: ["Bezier", {curveness: 175}],
        connectorStyle: {strokeWidth: 10, stroke: 'blue'}
    };

    //Define which point are source 
    // var synch1Endpoint = j.addEndpoint(widget1, { anchor:"TopCenter" }, sourceEndpointOptions );
    // var synch2Endpoint = j.addEndpoint(widget1, { anchor:"BottomCenter" }, targetEndpointOptions );

    // var synch1Endpoint = j.addEndpoint(widget2, { anchor:"TopCenter" }, sourceEndpointOptions );
    // var synch2Endpoint = j.addEndpoint(widget2, { anchor:"BottomCenter" }, targetEndpointOptions );
  
    // j.draggable(widget1, {containment:true})
    // j.draggable(widget2, {containment:true})
}

//This is the main execution entry point of the whole JS application code
jsPlumb.ready(function () {
    //...           
    // jsPlumb has loaded, related init code goes here
    //...  
    initJsPlumbStuff();
    initSynchEduca();
});

