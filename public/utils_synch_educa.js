var SynchEduca = SynchEduca || {};

SynchEduca.Utils = {
    findAndRemoveFromArray: function (array, element) {
        for (var i = 0; i < array.length - 1; i++) {
            if (array[i] === element) {
                return array.splice(i, 1);
            }
        }
    },
    compareStatement: function (widgetStatement, statement) {
        return widgetStatement.substring(widgetStatement.indexOf(".") + 2) === statement;
    }
};