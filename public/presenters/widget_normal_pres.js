var SynchEduca = SynchEduca || {};

SynchEduca.PresenterWidgetNormal = function (viewWidgetNormal, widgetData) {
    this.viewWidgetNormal = viewWidgetNormal;
    this.model = new SynchEduca.ModelWidgetNormal(widgetData);
    this.scheduler = SynchEduca.scheduler;
    this.scheduler.registerWidget(this);
    this.addEndpoints();
    viewWidgetNormal.attachDelete();
};

SynchEduca.PresenterWidgetNormal.prototype.addEndpoints = function () {
    var widgetData = this.model.getWidgetData();
    this.viewWidgetNormal.addTopEndpoint(widgetData.name);
    this.viewWidgetNormal.addBottomEndpoint(widgetData.name);
    this.addStatementEndpoints();
};

SynchEduca.PresenterWidgetNormal.prototype.addStatementEndpoints = function () {
    var widgetData = this.model.getWidgetData();

    for (let i = 0; i < widgetData.statementsType.length; i++) {
        if (widgetData.statementsType[i] === SynchEduca.ModelCreateComponent.STATEMENT_TYPE_SYNCH) {
            this.conditionalAddEndpointByStatmentType(
                widgetData.statements[i], widgetData.name, i);
        }
    }
};

SynchEduca.PresenterWidgetNormal.prototype.conditionalAddEndpointByStatmentType = function (
    statementObject, widgetId, statementPosition) {
    if (SynchEduca.Utils.compareStatement(statementObject, SynchEduca.ModelCreateComponent.STATEMENT_AWAIT_LATCH)) {
        this.viewWidgetNormal.addStatementLatchAwaitEndpoint(widgetId, statementPosition);
    } else if (SynchEduca.Utils.compareStatement(statementObject, SynchEduca.ModelCreateComponent.STATEMENT_COUNTDOWN_LATCH)) {
        this.viewWidgetNormal.addStatementLatchCountDownEndpoint(widgetId, statementPosition);
    } else if (SynchEduca.Utils.compareStatement(statementObject, SynchEduca.ModelCreateComponent.STATEMENT_AWAIT_BARRIER)) {
        this.viewWidgetNormal.addStatementBarrierAwaitEndpoint(widgetId, statementPosition);
    } else if (SynchEduca.Utils.compareStatement(statementObject, SynchEduca.ModelCreateComponent.STATEMENT_ONZERO_BARRIER)) {
        this.viewWidgetNormal.addStatementBarrierOnZeroEndpoint(widgetId, statementPosition);
    // Special check for semaphore, check if first part of the statement exists, sufix  part is variable to the number of tokens
    } else if (statementObject.indexOf(SynchEduca.ModelCreateComponent.STATEMENT_RELEASE_SEMAPHORE) >= 0) {
        this.viewWidgetNormal.addStatementSemaphoreReleaseEndpoint(widgetId, statementPosition);
    // Special check for semaphore, check if first part of the statement exists, sufix  part is variable to the number of tokens
    } else if (statementObject.indexOf(SynchEduca.ModelCreateComponent.STATEMENT_ACQUIRE_SEMAPHORE) >= 0) {
        this.viewWidgetNormal.addStatementSemaphoreAcquireEndpoint(widgetId, statementPosition);
    } else if (SynchEduca.Utils.compareStatement(statementObject, SynchEduca.ModelCreateComponent.STATEMENT_REGISTER_PHASER)) {
        this.viewWidgetNormal.addStatementPhaserRegisterEndpoint(widgetId, statementPosition);
    } else if (SynchEduca.Utils.compareStatement(statementObject, SynchEduca.ModelCreateComponent.STATEMENT_ARRIVE_DEREGISTER_PHASER)) {
        this.viewWidgetNormal.addStatementPhaserArriveDeregisterEndpoint(widgetId, statementPosition);
    } else if (SynchEduca.Utils.compareStatement(statementObject, SynchEduca.ModelCreateComponent.STATEMENT_ARRIVE_PHASER)) {
        this.viewWidgetNormal.addStatementPhaserArriveEndpoint(widgetId, statementPosition);
    } else if (SynchEduca.Utils.compareStatement(statementObject, SynchEduca.ModelCreateComponent.STATEMENT_ARRIVE_AWAIT_ADVANCE_PHASER)) {
        this.viewWidgetNormal.addStatementPhaserArriveAwaitAdvanceEndpoint(widgetId, statementPosition);
    } else if (SynchEduca.Utils.compareStatement(statementObject, SynchEduca.ModelCreateComponent.STATEMENT_AWAIT_ADVANCE_PHASER)) {
        this.viewWidgetNormal.addStatementPhaserAwaitAdvanceEndpoint(widgetId, statementPosition);
    }
}


SynchEduca.PresenterWidgetNormal.prototype.onDeleteWidget = function () {
    this.viewWidgetNormal.removeConnections();
    this.viewWidgetNormal.deleteWidgetUi(this.getWidgetId());
    this.scheduler.unregisterWidget(this);
};

SynchEduca.PresenterWidgetNormal.prototype.resetExecution = function () {
    this.model.resetProgramCounter();
    this.model.resetWorker();
};

SynchEduca.PresenterWidgetNormal.prototype.getWidgetId = function () {
    return this.model.getWidgetData().name;
};

//Specific interface exposed to scheduler
SynchEduca.PresenterWidgetNormal.prototype.getProgramCounter = function () {
    return this.model.getProgramCounter();
};

SynchEduca.PresenterWidgetNormal.prototype.incrementProgramCounter = function () {
    return this.model.incrementProgramCounter();
};

//Specific part - Processing statements
SynchEduca.PresenterWidgetNormal.prototype.executeNextStatement = function () {
    if (this.checkIfReachedEnd()) {
        this.model.resetProgramCounter();
        var data = {completed: true};
        this.scheduler.updateFromStatement(data, this);
    } else {
        this.proceedExecuteStatement();
    }
};

SynchEduca.PresenterWidgetNormal.prototype.checkIfReachedEnd = function () {
    return this.model.getProgramCounter() >= this.model.getNumberOfStatements()
};

SynchEduca.PresenterWidgetNormal.prototype.proceedExecuteStatement = function () {
    var _this = this;
    if (SynchEduca.Utils.compareStatement(this.model.getCurrentStatement(), SynchEduca.ModelCreateComponent.STATEMENT_COUNTDOWN_LATCH)) {
        this.model.sendMsgToWorker("executeNext", function (msg) {
            if (msg.success) {
                _this.showOutput();
                var data = {latchCountDown: true};
                _this.scheduler.updateFromStatement(data, _this);
            }
        });
    } else if (SynchEduca.Utils.compareStatement(this.model.getCurrentStatement(), SynchEduca.ModelCreateComponent.STATEMENT_AWAIT_LATCH)) {
        this.model.sendMsgToWorker("executeNext", function (msg) {
            if (msg.success) {
                _this.showOutput();
                var data = {latchAwait: true};
                _this.scheduler.updateFromStatement(data, _this);
            }
        });
    } else if (SynchEduca.Utils.compareStatement(this.model.getCurrentStatement(), SynchEduca.ModelCreateComponent.STATEMENT_AWAIT_BARRIER)) {
        this.model.sendMsgToWorker("executeNext", function (msg) {
            if (msg.success) {
                _this.showOutput();
                var data = {barrierAwait: true};
                _this.scheduler.updateFromStatement(data, _this);
            }
        });
    } else if (SynchEduca.Utils.compareStatement(this.model.getCurrentStatement(), SynchEduca.ModelCreateComponent.STATEMENT_ONZERO_BARRIER)) {
        this.model.sendMsgToWorker("executeNext", function (msg) {
            if (msg.success) {
                _this.showOutput();
                var data = {barrierOnZero: true};
                _this.scheduler.updateFromStatement(data, _this);
            }
        });
    // Special check for semaphore, check if first part of the statement exists, sufix  part is variable to the number of tokens 
    } else if (this.model.getCurrentStatement().indexOf(SynchEduca.ModelCreateComponent.STATEMENT_ACQUIRE_SEMAPHORE) >= 0) {
        this.model.sendMsgToWorker("executeNext", function (msg) {
            if (msg.success) {
                _this.showOutput();
                var data = {semaphoreAcquire: true, permits: _this.model.getCurrentParam()};
                _this.scheduler.updateFromStatement(data, _this);
            }
        });
    // Special check for semaphore, check if first part of the statement exists, sufix  part is variable to the number of tokens
    } else if (this.model.getCurrentStatement().indexOf(SynchEduca.ModelCreateComponent.STATEMENT_RELEASE_SEMAPHORE) >= 0) {
        this.model.sendMsgToWorker("executeNext", function (msg) {
            if (msg.success) {
                _this.showOutput();
                var data = {semaphoreRelease: true, permits: _this.model.getCurrentParam()};
                _this.scheduler.updateFromStatement(data, _this);
            }
        });
    } else if (SynchEduca.Utils.compareStatement(this.model.getCurrentStatement(), SynchEduca.ModelCreateComponent.STATEMENT_REGISTER_PHASER)) {
        this.model.sendMsgToWorker("executeNext", function (msg) {
            if (msg.success) {
                _this.showOutput();
                var data = {phaserRegister: true};
                _this.scheduler.updateFromStatement(data, _this);
            }
        });
    } else if (SynchEduca.Utils.compareStatement(this.model.getCurrentStatement(), SynchEduca.ModelCreateComponent.STATEMENT_ARRIVE_DEREGISTER_PHASER)) {
        this.model.sendMsgToWorker("executeNext", function (msg) {
            if (msg.success) {
                _this.showOutput();
                var data = {phaserArriveDeregister: true};
                _this.scheduler.updateFromStatement(data, _this);
            }
        });
    } else if (SynchEduca.Utils.compareStatement(this.model.getCurrentStatement(), SynchEduca.ModelCreateComponent.STATEMENT_ARRIVE_PHASER)) {
        this.model.sendMsgToWorker("executeNext", function (msg) {
            if (msg.success) {
                _this.showOutput();
                var data = {phaserArrive: true};
                _this.scheduler.updateFromStatement(data, _this);
            }
        });
    } else if (SynchEduca.Utils.compareStatement(this.model.getCurrentStatement(), SynchEduca.ModelCreateComponent.STATEMENT_AWAIT_ADVANCE_PHASER)) {
        this.model.sendMsgToWorker("executeNext", function (msg) {
            if (msg.success) {
                _this.showOutput();
                var data = {phaserAwaitAdvance: true};
                _this.scheduler.updateFromStatement(data, _this);
            }
        });
    } else if (SynchEduca.Utils.compareStatement(this.model.getCurrentStatement(), SynchEduca.ModelCreateComponent.STATEMENT_ARRIVE_AWAIT_ADVANCE_PHASER)) {
        this.model.sendMsgToWorker("executeNext", function (msg) {
            if (msg.success) {
                _this.showOutput();
                var data = {phaserArriveAwaitAdvance: true};
                _this.scheduler.updateFromStatement(data, _this);
            }
        });
    } else {
        this.model.sendMsgToWorker("executeNext", function (msg) {
            _this.onFinishedNormalStatement(msg);
        });
    }
};

SynchEduca.PresenterWidgetNormal.prototype.onFinishedNormalStatement = function (msg) {
    if (msg.success) {
        this.showOutput();
        this.model.incrementProgramCounter();
        this.executeNextStatement();
    }
};

SynchEduca.PresenterWidgetNormal.prototype.showOutput = function () {
    this.viewWidgetNormal.showOutput("<b>" + this.model.getWidgetData().name + "</b>: " + this.model.getCurrentStatement());
};