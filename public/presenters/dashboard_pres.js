var SynchEduca = SynchEduca || {};

SynchEduca.PresenterDashboard = function (viewDashboard) {
    this.viewDashboard = viewDashboard;
    this.model = SynchEduca.connectionsModel;
    SynchEduca.scheduler.addConnectionsModel(this.model);
};

SynchEduca.PresenterDashboard.prototype.onConnectionEstablished = function (connection) {
    this.model.addConnection(connection);
};

SynchEduca.PresenterDashboard.prototype.addTopEndpoint = function (endpointId, widgetId, jsPlumbEndpoint) {
    return this.model.addTopEndpoint(endpointId, widgetId, jsPlumbEndpoint);
};

SynchEduca.PresenterDashboard.prototype.addBottomEndpoint = function (endpointId, widgetId, jsPlumbEndpoint) {
    return this.model.addBottomEndpoint(endpointId, widgetId, jsPlumbEndpoint);
};

SynchEduca.PresenterDashboard.prototype.addStatementEndpoint = function (endpointId, widgetId, positionId, jsPlumbEndpoint) {
    return this.model.addStatementEndpoint(endpointId, widgetId, positionId, jsPlumbEndpoint);
};

SynchEduca.PresenterDashboard.prototype.checkIfExists = function (jsPlumbEndpointId) {
    return this.model.checkIfExists(jsPlumbEndpointId);
};