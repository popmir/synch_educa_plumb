var SynchEduca = SynchEduca || {};

SynchEduca.PresenterBarrierWidget = function (viewBarrierWidget, widgetData) {
    this.connectionsModel = SynchEduca.connectionsModel; //inject it from outside (no DI tool is used, so do it manually like this)
    this.viewBarrierWidget = viewBarrierWidget;
    this.model = new SynchEduca.ModelBarrierWidget(widgetData);
    this.scheduler = SynchEduca.scheduler;
    this.scheduler.registerBarrier(this);
    this.addEndpoints();
    viewBarrierWidget.attachDelete();
    this.resetExecution();
};

SynchEduca.PresenterBarrierWidget.prototype.addEndpoints = function () {
    this.addBarrierAwaitEndpoint();
    this.addBarrierOnZeroEndpoint();
};

SynchEduca.PresenterBarrierWidget.prototype.addBarrierAwaitEndpoint = function () {
    var jsPlumbEndpoint = this.viewBarrierWidget.addBarrierAwaitEndpoint(this.getWidgetId());
    if (this.connectionsModel.checkIfExists(jsPlumbEndpoint.getId())) {
        SynchEduca.jsPlumbInstance.deleteEndpoint(jsPlumbEndpoint);
        setTimeout(() => {
            this.addBarrierAwaitEndpoint();
        }, SynchEduca.ViewDashboard.DELAY_ADDING_ENDPOINT);
    } else {    
        this.endpointAwait = this.connectionsModel.addBarrierAwaitEndpoint(jsPlumbEndpoint.getId(), this.getWidgetId(), jsPlumbEndpoint);
    }
}

SynchEduca.PresenterBarrierWidget.prototype.addBarrierOnZeroEndpoint = function () {
    var jsPlumbEndpoint = this.viewBarrierWidget.addBarrierOnZeroEndpoint(this.getWidgetId());
    if (this.connectionsModel.checkIfExists(jsPlumbEndpoint.getId())) {
        SynchEduca.jsPlumbInstance.deleteEndpoint(jsPlumbEndpoint);
        setTimeout(() => {
            this.addBarrierOnZeroEndpoint();
        }, SynchEduca.ViewDashboard.DELAY_ADDING_ENDPOINT);
    } else {    
        this.endpointOnZero = this.connectionsModel.addBarrierOnZeroEndpoint(jsPlumbEndpoint.getId(), this.getWidgetId(), jsPlumbEndpoint);
    }
}

SynchEduca.PresenterBarrierWidget.prototype.onDeleteWidget = function () {
    this.scheduler.unregisterBarrier(this);
    this.viewBarrierWidget.removeConnections();
    this.viewBarrierWidget.deleteWidgetUi();
};

SynchEduca.PresenterBarrierWidget.prototype.executeAwait = function () {
    console.log("Executing await on " + this.getWidgetId());
    if (this.model.getCurrentCount() > 0) {
        var newCount = this.model.decrementCount();
        this.viewBarrierWidget.showCount(this.getWidgetId(), newCount);
        if (newCount <= 0) {
            this.scheduler.updateUnblockBarrier(this);
            this.resetExecution();
        }
    } else {
        alert(SynchEduca.strings.MSG_COUNT_ZERO);
    }
};

SynchEduca.PresenterBarrierWidget.prototype.isBlocking = function (widgetId, count) {
    return this.model.getCurrentCount() > 0;
};

SynchEduca.PresenterBarrierWidget.prototype.resetExecution = function () {
    this.model.resetCount();
    this.viewBarrierWidget.showCount(this.getWidgetId(), this.model.getCurrentCount());
};

SynchEduca.PresenterBarrierWidget.prototype.getWidgetId = function () {
    return this.model.getWidgetData().name;
};

SynchEduca.PresenterBarrierWidget.prototype.getEndpointAwait = function () {
    return this.endpointAwait;
};

SynchEduca.PresenterBarrierWidget.prototype.getEndpointOnZero = function () {
    return this.endpointOnZero;
};