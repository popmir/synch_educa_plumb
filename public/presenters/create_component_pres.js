var SynchEduca = SynchEduca || {};

SynchEduca.PresenterCreateComponent = function (viewCreateComponent, viewDashboard) {
    this.viewCreateComponent = viewCreateComponent;
    this.viewDashboard = viewDashboard;
    this.model = new SynchEduca.ModelCreateComponent();
    this.renderer = new SynchEduca.WidgetFromTemplate();
    this.showInitialData();
};

SynchEduca.PresenterCreateComponent.prototype.showInitialData = function () {
    //TODO read initial data from model and set it to create_component viewCreateComponent
    // for example widgetName after refresh has stale value in input box and on preview
    // the same stands for Latch initValue

    this.viewCreateComponent.showInitialMechanismSelection();
    this.viewCreateComponent.onLatchCountChange(this.model.getPreviewLatchData().initValue);
    this.viewCreateComponent.onBarrierCountChange(this.model.getPreviewBarrierData().initValue);
    this.viewCreateComponent.onSemaphoreCountChange(this.model.getPreviewSemaphoreData().initValue);
    this.viewCreateComponent.onPhaserCountChange(this.model.getPreviewPhaserData().initValue);
};

SynchEduca.PresenterCreateComponent.prototype.addSynchStatement = function (statement) {
    var statementCaption = this.model.addSynchStatement(statement);
    this.viewCreateComponent.addStatement(statementCaption);
};

SynchEduca.PresenterCreateComponent.prototype.addSynchStatementExtended = function (statement, param) {
    var statementCaption = this.model.addSynchStatementExtended(statement, param);
    this.viewCreateComponent.addStatement(statementCaption);
};

SynchEduca.PresenterCreateComponent.prototype.onAddPlainStatement = function () {
    var statementCaption = this.model.addPlainStatement(SynchEduca.ModelCreateComponent.STATEMENT_PLAIN);
    this.viewCreateComponent.addStatement(statementCaption);
};

SynchEduca.PresenterCreateComponent.prototype.onAddLatchCountDownStatement = function () {
    this.addSynchStatement(SynchEduca.ModelCreateComponent.STATEMENT_COUNTDOWN_LATCH);
};

SynchEduca.PresenterCreateComponent.prototype.onAddLatchAwaitStatement = function () {
    this.addSynchStatement(SynchEduca.ModelCreateComponent.STATEMENT_AWAIT_LATCH);
};

SynchEduca.PresenterCreateComponent.prototype.onAddBarrierAwaitStatement = function () {
    this.addSynchStatement(SynchEduca.ModelCreateComponent.STATEMENT_AWAIT_BARRIER);
};

SynchEduca.PresenterCreateComponent.prototype.onAddBarrierOnZeroStatement = function () {
    this.addSynchStatement(SynchEduca.ModelCreateComponent.STATEMENT_ONZERO_BARRIER);
};

SynchEduca.PresenterCreateComponent.prototype.onAddSemaphoreReleaseStatement = function (permits) {
    this.addSynchStatementExtended(SynchEduca.ModelCreateComponent.STATEMENT_RELEASE_SEMAPHORE, permits);
};

SynchEduca.PresenterCreateComponent.prototype.onAddSemaphoreAcquireStatement = function (permits) {
    this.addSynchStatementExtended(SynchEduca.ModelCreateComponent.STATEMENT_ACQUIRE_SEMAPHORE, permits);
};

SynchEduca.PresenterCreateComponent.prototype.onAddPhaserRegisterStatement = function () {
    this.addSynchStatement(SynchEduca.ModelCreateComponent.STATEMENT_REGISTER_PHASER);
};

SynchEduca.PresenterCreateComponent.prototype.onAddPhaserArriveDeregisterStatement = function () {
    this.addSynchStatement(SynchEduca.ModelCreateComponent.STATEMENT_ARRIVE_DEREGISTER_PHASER);
};

SynchEduca.PresenterCreateComponent.prototype.onAddPhaserArriveStatement = function () {
    this.addSynchStatement(SynchEduca.ModelCreateComponent.STATEMENT_ARRIVE_PHASER);
};

SynchEduca.PresenterCreateComponent.prototype.onAddPhaserAwaitAdvanceStatement = function () {
    this.addSynchStatement(SynchEduca.ModelCreateComponent.STATEMENT_AWAIT_ADVANCE_PHASER);
};

SynchEduca.PresenterCreateComponent.prototype.onAddPhaserArriveAwaitAdvanceStatement = function () {
    this.addSynchStatement(SynchEduca.ModelCreateComponent.STATEMENT_ARRIVE_AWAIT_ADVANCE_PHASER);
};

SynchEduca.PresenterCreateComponent.prototype.onRemoveLastStatement = function () {
    this.model.removeLastStatement();
    this.viewCreateComponent.removeLastStatement();
};

SynchEduca.PresenterCreateComponent.prototype.onClearAllStatements = function () {
    this.model.clearAllStatements();
    this.viewCreateComponent.clearAllStatements();
};

SynchEduca.PresenterCreateComponent.prototype.onAddWidget = function (widgetName) {
    var widgetData = this.model.getExampleWorkerWidgetData();
    widgetData.name = widgetName;
    if (widgetData.statements.length > 0) {
        var renderedWidgetHtml = this.renderer.renderNormalWidget(widgetData);
        this.viewDashboard.showWidget(renderedWidgetHtml);
        this.viewDashboard.makeWidgetDraggable(widgetData.name);
        this.viewDashboard.attachWidgetView(widgetData);
    }
};

SynchEduca.PresenterCreateComponent.prototype.onAddLatchWidget = function (latchInitialCount) {
    this.model.setLatchInitialValue(latchInitialCount);
    var widgetData = this.model.getPreviewLatchData();
    var renderedWidgetHtml = this.renderer.renderLatchWidget(widgetData);
    this.viewDashboard.showWidget(renderedWidgetHtml);
    this.viewDashboard.makeWidgetDraggable(widgetData.name);
    this.viewDashboard.attachLatchView(widgetData);
    this.model.incrementLatchNameSuffix();
};

SynchEduca.PresenterCreateComponent.prototype.onAddBarrierWidget = function (initialCount) {
    this.model.setBarrierInitialValue(initialCount);
    var widgetData = this.model.getPreviewBarrierData();
    var renderedWidgetHtml = this.renderer.renderBarrierWidget(widgetData);
    this.viewDashboard.showWidget(renderedWidgetHtml);
    this.viewDashboard.makeWidgetDraggable(widgetData.name);
    this.viewDashboard.attachBarrierView(widgetData);
    this.model.incrementBarrierNameSuffix();
};

SynchEduca.PresenterCreateComponent.prototype.onAddSemaphoreWidget = function (initialCount) {
    this.model.setSemaphoreInitialValue(initialCount);
    var widgetData = this.model.getPreviewSemaphoreData();
    var renderedWidgetHtml = this.renderer.renderSemaphoreWidget(widgetData);
    this.viewDashboard.showWidget(renderedWidgetHtml);
    this.viewDashboard.makeWidgetDraggable(widgetData.name);
    this.viewDashboard.attachSemaphoreView(widgetData);
    this.model.incrementSemaphoreNameSuffix();
};

SynchEduca.PresenterCreateComponent.prototype.onAddPhaserWidget = function (initialCount) {
    this.model.setPhaserInitialValue(initialCount);
    var widgetData = this.model.getPreviewPhaserData();
    var renderedWidgetHtml = this.renderer.renderPhaserWidget(widgetData);
    this.viewDashboard.showWidget(renderedWidgetHtml);
    this.viewDashboard.makeWidgetDraggable(widgetData.name);
    this.viewDashboard.attachPhaserView(widgetData);
    this.model.incrementPhaserNameSuffix();
};