var SynchEduca = SynchEduca || {};

SynchEduca.PresenterLatchWidget = function (viewLatchWidget, widgetData) {
    this.connectionsModel = SynchEduca.connectionsModel; //inject it from outside (no DI tool is used, so do it manually like this)
    this.viewLatchWidget = viewLatchWidget;
    this.model = new SynchEduca.ModelLatchWidget(widgetData);
    this.scheduler = SynchEduca.scheduler;
    this.scheduler.registerLatch(this);
    viewLatchWidget.setMaxCountDownConnections(widgetData.initValue);
    this.addEndpoints();
    viewLatchWidget.attachDelete();
    this.resetExecution();
};

SynchEduca.PresenterLatchWidget.prototype.addEndpoints = function () {
    this.addLatchAwaitEndpoint();
    this.addLatchCountDownEndpoint();
};

SynchEduca.PresenterLatchWidget.prototype.addLatchCountDownEndpoint = function () {
    var jsPlumbEndpoint = this.viewLatchWidget.addLatchCountDownEndpoint(this.getWidgetId());
    if (this.connectionsModel.checkIfExists(jsPlumbEndpoint.getId())) {
        SynchEduca.jsPlumbInstance.deleteEndpoint(jsPlumbEndpoint);
        setTimeout(() => {
            this.addLatchCountDownEndpoint();
        }, SynchEduca.ViewDashboard.DELAY_ADDING_ENDPOINT);
    } else {    
        this.endpointCountDown = this.connectionsModel.addLatchCountDownEndpoint(jsPlumbEndpoint.getId(), this.getWidgetId(), jsPlumbEndpoint);
    }
}

SynchEduca.PresenterLatchWidget.prototype.addLatchAwaitEndpoint = function () {
    var jsPlumbEndpoint = this.viewLatchWidget.addLatchAwaitEndpoint(this.getWidgetId());
    if (this.connectionsModel.checkIfExists(jsPlumbEndpoint.getId())) {
        SynchEduca.jsPlumbInstance.deleteEndpoint(jsPlumbEndpoint);
        setTimeout(() => {
            this.addLatchAwaitEndpoint();
        }, SynchEduca.ViewDashboard.DELAY_ADDING_ENDPOINT);
    } else {     
        this.endpointAwait = this.connectionsModel.addLatchAwaitEndpoint(jsPlumbEndpoint.getId(), this.getWidgetId(), jsPlumbEndpoint);
    }
}

SynchEduca.PresenterLatchWidget.prototype.onDeleteWidget = function () {
    this.scheduler.unregisterLatch(this);
    this.viewLatchWidget.removeConnections();
    this.viewLatchWidget.deleteWidgetUi();
};

SynchEduca.PresenterLatchWidget.prototype.executeCountDown = function () {
    console.log("Executing count down on " + this.getWidgetId());
    if (this.model.getCurrentCount() > 0) {
        var newCount = this.model.decrementCount();
        this.viewLatchWidget.showCount(this.getWidgetId(), newCount);
        if (newCount <= 0) {
            this.scheduler.updateUnblockLatch(this);
        }
    } else {
        alert(SynchEduca.strings.MSG_COUNT_ZERO);
    }
};

SynchEduca.PresenterLatchWidget.prototype.isBlocking = function (widgetId, count) {
    return this.model.getCurrentCount() > 0;
};

SynchEduca.PresenterLatchWidget.prototype.resetExecution = function () {
    this.model.resetCount();
    this.viewLatchWidget.showCount(this.getWidgetId(), this.model.getCurrentCount());
};

SynchEduca.PresenterLatchWidget.prototype.getWidgetId = function () {
    return this.model.getWidgetData().name;
};

SynchEduca.PresenterLatchWidget.prototype.getEndpointCountDown = function () {
    return this.endpointCountDown;
};

SynchEduca.PresenterLatchWidget.prototype.getEndpointAwait = function () {
    return this.endpointAwait;
};

