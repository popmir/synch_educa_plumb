var SynchEduca = SynchEduca || {};

SynchEduca.PresenterPhaserWidget = function (viewPhaserWidget, widgetData) {
    this.connectionsModel = SynchEduca.connectionsModel; //inject it from outside (no DI tool is used, so do it manually like this)
    this.viewPhaserWidget = viewPhaserWidget;
    this.model = new SynchEduca.ModelPhaserWidget(widgetData);
    this.scheduler = SynchEduca.scheduler;
    this.scheduler.registerPhaser(this);
    this.addEndpoints();
    viewPhaserWidget.attachDelete();
    this.resetExecution();
};

SynchEduca.PresenterPhaserWidget.prototype.addEndpoints = function () {
    this.addPhaserRegisterEndpoint();
    this.addPhaserArriveDeregisterEndpoint();
    this.addPhaserArriveEndpoint();
    this.addPhaserAwaitAdvanceEndpoint();
    this.addPhaserArriveAwaitAdvanceEndpoint();
};

SynchEduca.PresenterPhaserWidget.prototype.addPhaserRegisterEndpoint = function () {
    var jsPlumbEndpoint = this.viewPhaserWidget.addPhaserRegisterEndpoint(this.getWidgetId());
    if (this.connectionsModel.checkIfExists(jsPlumbEndpoint.getId())) {
        SynchEduca.jsPlumbInstance.deleteEndpoint(jsPlumbEndpoint);
        setTimeout(() => {
            this.addPhaserRegisterEndpoint();
        }, SynchEduca.ViewDashboard.DELAY_ADDING_ENDPOINT);
    } else {    
        this.endpointRegister = this.connectionsModel.addPhaserRegisterEndpoint(jsPlumbEndpoint.getId(), this.getWidgetId(), jsPlumbEndpoint);
    }
}

SynchEduca.PresenterPhaserWidget.prototype.addPhaserArriveDeregisterEndpoint = function () {
    var jsPlumbEndpoint = this.viewPhaserWidget.addPhaserArriveDeregisterEndpoint(this.getWidgetId());
    if (this.connectionsModel.checkIfExists(jsPlumbEndpoint.getId())) {
        SynchEduca.jsPlumbInstance.deleteEndpoint(jsPlumbEndpoint);
        setTimeout(() => {
            this.addPhaserArriveDeregisterEndpoint();
        }, SynchEduca.ViewDashboard.DELAY_ADDING_ENDPOINT);
    } else {    
        this.endpointArriveDeregister = this.connectionsModel.addPhaserArriveDeregisterEndpoint(jsPlumbEndpoint.getId(), this.getWidgetId(), jsPlumbEndpoint);
    }
}

SynchEduca.PresenterPhaserWidget.prototype.addPhaserArriveEndpoint = function () {
    var jsPlumbEndpoint = this.viewPhaserWidget.addPhaserArriveEndpoint(this.getWidgetId());
    if (this.connectionsModel.checkIfExists(jsPlumbEndpoint.getId())) {
        SynchEduca.jsPlumbInstance.deleteEndpoint(jsPlumbEndpoint);
        setTimeout(() => {
            this.addPhaserArriveEndpoint();
        }, SynchEduca.ViewDashboard.DELAY_ADDING_ENDPOINT);
    } else {    
        this.endpointArrive = this.connectionsModel.addPhaserArriveEndpoint(jsPlumbEndpoint.getId(), this.getWidgetId(), jsPlumbEndpoint);
    }
}

SynchEduca.PresenterPhaserWidget.prototype.addPhaserAwaitAdvanceEndpoint = function () {
    var jsPlumbEndpoint = this.viewPhaserWidget.addPhaserAwaitAdvanceEndpoint(this.getWidgetId());
    if (this.connectionsModel.checkIfExists(jsPlumbEndpoint.getId())) {
        SynchEduca.jsPlumbInstance.deleteEndpoint(jsPlumbEndpoint);
        setTimeout(() => {
            this.addPhaserAwaitAdvanceEndpoint();
        }, SynchEduca.ViewDashboard.DELAY_ADDING_ENDPOINT);
    } else {    
        this.endpointAwaitAdvance = this.connectionsModel.addPhaserAwaitAdvanceEndpoint(jsPlumbEndpoint.getId(), this.getWidgetId(), jsPlumbEndpoint);
    }
}

SynchEduca.PresenterPhaserWidget.prototype.addPhaserArriveAwaitAdvanceEndpoint = function () {
    var jsPlumbEndpoint = this.viewPhaserWidget.addPhaserArriveAwaitAdvanceEndpoint(this.getWidgetId());
    if (this.connectionsModel.checkIfExists(jsPlumbEndpoint.getId())) {
        SynchEduca.jsPlumbInstance.deleteEndpoint(jsPlumbEndpoint);
        setTimeout(() => {
            this.addPhaserArriveAwaitAdvanceEndpoint();
        }, SynchEduca.ViewDashboard.DELAY_ADDING_ENDPOINT);
    } else {    
        this.endpointArriveAwaitAdvance = this.connectionsModel.addPhaserArriveAwaitAdvanceEndpoint(jsPlumbEndpoint.getId(), this.getWidgetId(), jsPlumbEndpoint);
    }
}

SynchEduca.PresenterPhaserWidget.prototype.onDeleteWidget = function () {
    this.scheduler.unregisterPhaser(this);
    this.viewPhaserWidget.removeConnections();
    this.viewPhaserWidget.deleteWidgetUi();
};

SynchEduca.PresenterPhaserWidget.prototype.executeRegister = function () {
    console.log("Executing register on " + this.getWidgetId());
    this.model.incrementRegisteredCount();
    this.model.incrementUnarrivedCount();
    this.viewPhaserWidget.showCount(this.getWidgetId(), this.model.getCurrentRegisteredCount(), this.model.getCurrentUnarrivedCount());
};

SynchEduca.PresenterPhaserWidget.prototype.executeArriveDeregister = function () {
    console.log("Executing arrive deregister on " + this.getWidgetId());
    this.model.decrementRegisteredCount();
    this.model.decrementUnarrivedCount();
    this.viewPhaserWidget.showCount(this.getWidgetId(), this.model.getCurrentRegisteredCount(), this.model.getCurrentUnarrivedCount());
    if (!this.isBlocking()) {
        this.scheduler.updateUnblockPhaser(this);
        this.resetExecutionCount();
    }
};

SynchEduca.PresenterPhaserWidget.prototype.executeArrive = function () {
    console.log("Executing arrive on " + this.getWidgetId());
    this.model.decrementUnarrivedCount();
    this.viewPhaserWidget.showCount(this.getWidgetId(), this.model.getCurrentRegisteredCount(), this.model.getCurrentUnarrivedCount());
    if (!this.isBlocking()) {
        this.scheduler.updateUnblockPhaser(this);
        this.resetExecutionCount();
    }
};

SynchEduca.PresenterPhaserWidget.prototype.executeArriveAwaitAdvance = function () {
    console.log("Executing arrive await advance on " + this.getWidgetId());
    if (this.model.getCurrentUnarrivedCount() > 0) {
        this.model.decrementUnarrivedCount();
        this.viewPhaserWidget.showCount(this.getWidgetId(), this.model.getCurrentRegisteredCount(), this.model.getCurrentUnarrivedCount());
        if (!this.isBlocking()) {
            this.scheduler.updateUnblockPhaser(this);
            this.resetExecutionCount();
        }
    } else {
        alert(SynchEduca.strings.MSG_COUNT_ZERO);
    }
};

SynchEduca.PresenterPhaserWidget.prototype.isBlocking = function () {
    return this.model.getCurrentUnarrivedCount() > 0;
};

SynchEduca.PresenterPhaserWidget.prototype.resetExecution = function () {
    this.model.resetCount();
    this.viewPhaserWidget.showCount(this.getWidgetId(), this.model.getCurrentRegisteredCount(), this.model.getCurrentUnarrivedCount());
};

SynchEduca.PresenterPhaserWidget.prototype.resetExecutionCount = function () {
    this.model.resetExecutionCount();
    this.viewPhaserWidget.showCount(this.getWidgetId(), this.model.getCurrentRegisteredCount(), this.model.getCurrentUnarrivedCount());
};

SynchEduca.PresenterPhaserWidget.prototype.getWidgetId = function () {
    return this.model.getWidgetData().name;
};

SynchEduca.PresenterPhaserWidget.prototype.getEndpointRegister = function () {
    return this.endpointRegister;
};

SynchEduca.PresenterPhaserWidget.prototype.getEndpointArriveDeregister = function () {
    return this.endpointArriveDeregister;
};

SynchEduca.PresenterPhaserWidget.prototype.getEndpointArrive = function () {
    return this.endpointArrive;
};

SynchEduca.PresenterPhaserWidget.prototype.getEndpointAwaitAdvance = function () {
    return this.endpointAwaitAdvance;
};

SynchEduca.PresenterPhaserWidget.prototype.getEndpointArriveAwaitAdvance = function () {
    return this.endpointArriveAwaitAdvance;
};