var SynchEduca = SynchEduca || {};

SynchEduca.PresenterSemaphoreWidget = function (viewSemaphoreWidget, widgetData) {
    this.connectionsModel = SynchEduca.connectionsModel; //inject it from outside (no DI tool is used, so do it manually like this)
    this.viewSemaphoreWidget = viewSemaphoreWidget;
    this.model = new SynchEduca.ModelSemaphoreWidget(widgetData);
    this.scheduler = SynchEduca.scheduler;
    this.scheduler.registerSemaphore(this);
    this.addEndpoints();
    viewSemaphoreWidget.attachDelete();
    this.resetExecution();
};

SynchEduca.PresenterSemaphoreWidget.prototype.addEndpoints = function () {
    this.addSemaphoreReleaseEndpoint();
    this.addSemaphoreAcquireEndpoint();
};

SynchEduca.PresenterSemaphoreWidget.prototype.addSemaphoreAcquireEndpoint = function () {
    var jsPlumbEndpoint = this.viewSemaphoreWidget.addSemaphoreAcquireEndpoint(this.getWidgetId());
    if (this.connectionsModel.checkIfExists(jsPlumbEndpoint.getId())) {
        SynchEduca.jsPlumbInstance.deleteEndpoint(jsPlumbEndpoint);
        setTimeout(() => {
            this.addSemaphoreAcquireEndpoint();
        }, SynchEduca.ViewDashboard.DELAY_ADDING_ENDPOINT);
    } else {    
        this.endpointAcquire = this.connectionsModel.addSemaphoreAcquireEndpoint(jsPlumbEndpoint.getId(), this.getWidgetId(), jsPlumbEndpoint);
    }
}

SynchEduca.PresenterSemaphoreWidget.prototype.addSemaphoreReleaseEndpoint = function () {
    var jsPlumbEndpoint = this.viewSemaphoreWidget.addSemaphoreReleaseEndpoint(this.getWidgetId());

    if (this.connectionsModel.checkIfExists(jsPlumbEndpoint.getId())) {
        SynchEduca.jsPlumbInstance.deleteEndpoint(jsPlumbEndpoint);
        setTimeout(() => {
            this.addSemaphoreReleaseEndpoint();
        }, SynchEduca.ViewDashboard.DELAY_ADDING_ENDPOINT);
    } else {
        this.endpointRelease = this.connectionsModel.addSemaphoreReleaseEndpoint(jsPlumbEndpoint.getId(), this.getWidgetId(), jsPlumbEndpoint);
    }
}

SynchEduca.PresenterSemaphoreWidget.prototype.onDeleteWidget = function () {
    this.scheduler.unregisterSemaphore(this);
    this.viewSemaphoreWidget.removeConnections();
    this.viewSemaphoreWidget.deleteWidgetUi();
};

SynchEduca.PresenterSemaphoreWidget.prototype.executeRelease = function (permits) {
    console.log("Executing release on " + this.getWidgetId() + " with " + permits + " permits");
    var newCount = this.model.addPermits(permits);
    this.viewSemaphoreWidget.showCount(this.getWidgetId(), newCount);
    this.scheduler.updateUnblockSemaphore(this);
};

SynchEduca.PresenterSemaphoreWidget.prototype.executeAcquire = function (permits) {
    console.log("Executing acquire on " + this.getWidgetId() + " with " + permits + " permits");
    var newCount = this.model.removePermits(permits);
    this.viewSemaphoreWidget.showCount(this.getWidgetId(), newCount);
};

SynchEduca.PresenterSemaphoreWidget.prototype.isBlocking = function (permits) {
    return this.model.getCurrentCount() < permits;
};

SynchEduca.PresenterSemaphoreWidget.prototype.resetExecution = function () {
    this.model.resetCount();
    this.viewSemaphoreWidget.showCount(this.getWidgetId(), this.model.getCurrentCount());
};

SynchEduca.PresenterSemaphoreWidget.prototype.getWidgetId = function () {
    return this.model.getWidgetData().name;
};

SynchEduca.PresenterSemaphoreWidget.prototype.getEndpointRelease = function () {
    return this.endpointRelease;
};

SynchEduca.PresenterSemaphoreWidget.prototype.getEndpointAcquire = function () {
    return this.endpointAcquire;
};