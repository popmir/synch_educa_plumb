var SynchEduca = SynchEduca || {};

SynchEduca.templateWidgetHtml =
    `                <div class="group-container" id="{{=it.data.name}}" >
                    <div class="widget_top_bar">
                      <span class="widget_name" id="{{=it.data.name}}">{{=it.data.name}}</span>
                      <span class="widget_delete" id="widget_delete">X</span>
                    </div>
                    <ul>
                    {{~it.data.statements :element }}
                        <li>{{=element}}</li>
                    {{~}}
                    </ul>
                </div>
`;
SynchEduca.templateLatchHtml =
    `               <div class="group-container" id="{{=it.data.name}}">
                    <div class="widget_top_bar">
                      <span class="widget_name" id="{{=it.data.name}}Name">{{=it.data.name}}</span>
                      <span class="widget_delete" id="widget_delete">X</span> 
                    </div>
                    <div class="widget_middle_bar">
                        <span class="preview_init_value">Initial value:&nbsp;</span><span id="{{=it.data.name}}_latch_init_count">{{=it.data.initValue}}</span>
                    </div>
                    <div class="synch_content">
                      <div id="{{=it.data.name}}_latch_state_display" class="state_display_container">
                        <span>Latch count</span>
                        <span id="{{=it.data.name}}_latch_count" class="mechanism_state">{{=it.data.initValue}}</span>
                      </div>
                      <div class="synch_operations">
                        <div class="target_latch_count_down">
                          <span>Count down</span>
                        </div>
                        <div class="target_latch_await">
                          <span>Await</span>
                        </div>
                      </div>
                    </div>    
                </div>
`;

SynchEduca.templateBarrierHtml =
    `               <div class="group-container" id="{{=it.data.name}}">
                    <div class="widget_top_bar">
                      <span class="widget_name" id="{{=it.data.name}}Name">{{=it.data.name}}</span>
                      <span class="widget_delete" id="widget_delete">X</span> 
                    </div>
                    <div class="widget_middle_bar">
                        <span class="preview_init_value">Initial value:&nbsp;</span><span id="{{=it.data.name}}_barrier_init_count">{{=it.data.initValue}}</span>
                    </div>
                    <div class="synch_content">
                      <div id="{{=it.data.name}}_barrier_state_display" class="state_display_container">
                        <span>Barrier count</span>
                        <span id="{{=it.data.name}}_barrier_count" class="mechanism_state">{{=it.data.initValue}}</span>
                      </div>
                      <div class="synch_operations">
                        <div class="target_barrier_await">
                          <span>Await</span>
                        </div>
                        <div class="target_barrier_on_zero">
                          <span>OnZero</span>
                        </div>
                      </div>
                    </div>    
                </div>
`;
SynchEduca.templateSemaphoreHtml =
    `               <div class="group-container" id="{{=it.data.name}}">
                    <div class="widget_top_bar">
                      <span class="widget_name" id="{{=it.data.name}}Name">{{=it.data.name}}</span>
                      <span class="widget_delete" id="widget_delete">X</span> 
                    </div>
                    <div class="widget_middle_bar">
                        <span class="preview_init_value">Initial value:&nbsp;</span><span id="{{=it.data.name}}_semaphore_init_count">{{=it.data.initValue}}</span>
                    </div>
                    <div class="synch_content">
                      <div id="{{=it.data.name}}_semaphore_state_display" class="state_display_container">
                        <span>Semaphore permits</span>
                        <span id="{{=it.data.name}}_semaphore_count" class="mechanism_state">{{=it.data.initValue}}</span>
                      </div>
                      <div class="synch_operations">
                        <div class="target_semaphore_release">
                          <span>Release</span>
                        </div>
                        <div class="target_semaphore_acquire">
                          <span>Acquire</span>
                        </div>
                      </div>
                    </div>    
                </div>
`;
SynchEduca.templatePhaserHtml =
    `               <div class="group-container" id="{{=it.data.name}}">
                    <div class="widget_top_bar">
                      <span class="widget_name" id="{{=it.data.name}}Name">{{=it.data.name}}</span>
                      <span class="widget_delete" id="widget_delete">X</span> 
                    </div>
                    <div class="widget_middle_bar">
                        <span class="preview_init_value">Initial value:&nbsp;</span><span id="{{=it.data.name}}_phaser_init_count">{{=it.data.initValue}}</span>
                    </div>
                    <div class="synch_content">  
                      <div class="row">
                        <div class="col-6">
                            <div id="{{=it.data.name}}_phaser_state_display" class="state_display_container">
                                <span style="text-align: center">Registered parties</span>
                                <span id="{{=it.data.name}}_phaser_registered_count" class="mechanism_state">{{=it.data.initValue}}</span>
                            </div>
                        </div>
                        <div class="col-6">
                            <div id="{{=it.data.name}}_phaser_state_display" class="state_display_container">
                                <span style="text-align: center">Unarrived parties</span>
                                <span id="{{=it.data.name}}_phaser_unarrived_count" class="mechanism_state">{{=it.data.initValue}}</span>
                            </div>
                        </div>
                      </div>
                      <div class="synch_operations">
                        <div class="target_phaser_register">
                          <span>Register</span>
                        </div>
                        <div class="target_phaser_unregister">
                            <span>Arrive Await Advance</span>
                            <br>
                            <span>Await Advance</span>
                            <br>
                            <span>Arrive</span>
                            <br>
                            <span>Arrive Deregister</span>
                        </div>
                      </div>
                    </div>    
                </div>
`;

SynchEduca.WidgetFromTemplate = function () {
    //this.widgetTemplate = doT.template("<h1>Here is a sample template {{=it.foo}}</h1>");
    this.widgetTemplateRenderer = doT.template(SynchEduca.templateWidgetHtml);
    this.latchTemplateRenderer = doT.template(SynchEduca.templateLatchHtml);
    this.barrierTemplateRenderer = doT.template(SynchEduca.templateBarrierHtml);
    this.semaphoreTemplateRenderer = doT.template(SynchEduca.templateSemaphoreHtml);
    this.phaserTemplateRenderer = doT.template(SynchEduca.templatePhaserHtml);
};

SynchEduca.WidgetFromTemplate.prototype.renderNormalWidget = function (widgetData) {
    return this.widgetTemplateRenderer({data: widgetData});
};

SynchEduca.WidgetFromTemplate.prototype.renderLatchWidget = function (widgetData) {
    return this.latchTemplateRenderer({data: widgetData});
};

SynchEduca.WidgetFromTemplate.prototype.renderBarrierWidget = function (widgetData) {
    return this.barrierTemplateRenderer({data: widgetData});
};

SynchEduca.WidgetFromTemplate.prototype.renderSemaphoreWidget = function (widgetData) {
    return this.semaphoreTemplateRenderer({data: widgetData});
};

SynchEduca.WidgetFromTemplate.prototype.renderPhaserWidget = function (widgetData) {
    return this.phaserTemplateRenderer({data: widgetData});
};