var SynchEduca = SynchEduca || {};

SynchEduca.Scheduler = function () {
    this.schedulerModel = new SynchEduca.ModelScheduler();
};

SynchEduca.Scheduler.prototype.addConnectionsModel = function (dashboardConnectionsModel) {
    this.connectionsModel = dashboardConnectionsModel;
};

SynchEduca.Scheduler.prototype.registerLatch = function (widgetPresenter) {
    this.schedulerModel.registerLatch(widgetPresenter.getWidgetId(), widgetPresenter);
};

SynchEduca.Scheduler.prototype.unregisterLatch = function (widgetPresenter) {
    this.schedulerModel.unregisterLatch(widgetPresenter.getWidgetId());
    this.connectionsModel.removeAll(widgetPresenter.getWidgetId());
};

SynchEduca.Scheduler.prototype.registerBarrier = function (widgetPresenter) {
    this.schedulerModel.registerBarrier(widgetPresenter.getWidgetId(), widgetPresenter);
};

SynchEduca.Scheduler.prototype.unregisterBarrier = function (widgetPresenter) {
    this.schedulerModel.unregisterBarrier(widgetPresenter.getWidgetId());
    this.connectionsModel.removeAll(widgetPresenter.getWidgetId());
};

SynchEduca.Scheduler.prototype.registerSemaphore = function (widgetPresenter) {
    this.schedulerModel.registerSemaphore(widgetPresenter.getWidgetId(), widgetPresenter);
};

SynchEduca.Scheduler.prototype.unregisterSemaphore = function (widgetPresenter) {
    this.schedulerModel.unregisterSemaphore(widgetPresenter.getWidgetId());
    this.connectionsModel.removeAll(widgetPresenter.getWidgetId());
};

SynchEduca.Scheduler.prototype.registerPhaser = function (widgetPresenter) {
    this.schedulerModel.registerPhaser(widgetPresenter.getWidgetId(), widgetPresenter);
};

SynchEduca.Scheduler.prototype.unregisterPhaser = function (widgetPresenter) {
    this.schedulerModel.unregisterPhaser(widgetPresenter.getWidgetId());
    this.connectionsModel.removeAll(widgetPresenter.getWidgetId());
};

SynchEduca.Scheduler.prototype.registerWidget = function (widgetPresenter) {
    this.schedulerModel.registerWidget(widgetPresenter.getWidgetId(), widgetPresenter);
};

SynchEduca.Scheduler.prototype.unregisterWidget = function (widgetPresenter) {
    this.schedulerModel.removeFromRunningQueue(widgetPresenter);
    this.schedulerModel.removeFromReadyQueue(widgetPresenter);
    this.schedulerModel.removeFromAllBlockedQueues(widgetPresenter);
    this.schedulerModel.unregisterWidget(widgetPresenter.getWidgetId());
    this.connectionsModel.removeAll(widgetPresenter.getWidgetId());
};

SynchEduca.Scheduler.prototype.resetExecutions = function () {
    this.schedulerModel.iterateAllWidgets(function (widgetPresenter) {
        widgetPresenter.resetExecution();
    });
    this.schedulerModel.iterateAllMechanisms(function (widgetPresenter) {
        widgetPresenter.resetExecution();
    });
};

SynchEduca.Scheduler.prototype.transferToReadyQueue = function () {
    var _this = this;
    this.schedulerModel.iterateAllWidgets(function (widgetPresenter) {
        _this.schedulerModel.pushToReadyQueue(widgetPresenter);
    });
};

SynchEduca.Scheduler.prototype.startExecution = function () {
    var widgetPresenter = {};
    this.resetExecutions();
    this.schedulerModel.resetQueues();
    this.transferToReadyQueue();
    while (widgetPresenter = this.schedulerModel.popFromReadyQueue()) {
        //Check if widget execution depends on another widget that needs to finish
        var endpoint = this.connectionsModel.getSourceConnectedToEndpoint(widgetPresenter.getWidgetId(), SynchEduca.ModelDashboardConnections.TOP);
        if (endpoint) {
            var idOfBlocker = endpoint.toString();
            this.schedulerModel.pushToBlockedQueue(idOfBlocker, widgetPresenter);
        } else {
            this.schedulerModel.pushToRunningQueue(widgetPresenter);
            widgetPresenter.executeNextStatement();
        }
    }
};

SynchEduca.Scheduler.prototype.updateUnblockLatch = function (sourceWidgetPresenter) {
    var endpoint = sourceWidgetPresenter.getEndpointAwait();
    this.unblockAllDependant(endpoint.toString());
};

SynchEduca.Scheduler.prototype.updateUnblockBarrier = function (sourceWidgetPresenter) {
    var endpointAwait = sourceWidgetPresenter.getEndpointAwait();
    var endpointOnZero = sourceWidgetPresenter.getEndpointOnZero();
    this.unblockAllDependant(endpointAwait.toString());
    this.unblockAllDependant(endpointOnZero.toString());
};

SynchEduca.Scheduler.prototype.updateUnblockSemaphore = function (sourceWidgetPresenter) {
    var endpoint = sourceWidgetPresenter.getEndpointAcquire();
    var idOfBlocker = endpoint.toString();

    var data = {};
    while (data = this.schedulerModel.peekFirstInBlockedQueue(idOfBlocker)) {
        var permits = sourceWidgetPresenter.model.getCurrentCount();
        if (data.permits <= permits) {
            var semaphorePresenter = this.schedulerModel.getSemaphorePresenter(endpoint.widgetId);
            semaphorePresenter.executeAcquire(data.permits);

            this.schedulerModel.pushToRunningQueue(data.widgetPresenter);
            this.schedulerModel.dequeueFromBlockedQueue(idOfBlocker);
            data.widgetPresenter.executeNextStatement();
        } else break;
    }
};

SynchEduca.Scheduler.prototype.updateUnblockPhaser = function (sourceWidgetPresenter) {
    var endpointAwaitAdvance = sourceWidgetPresenter.getEndpointAwaitAdvance();
    var endpointArriveAwaitAdvance = sourceWidgetPresenter.getEndpointArriveAwaitAdvance();
    this.unblockAllDependant(endpointAwaitAdvance.toString());
    this.unblockAllDependant(endpointArriveAwaitAdvance.toString());
};

SynchEduca.Scheduler.prototype.updateFromStatement = function (data, sourceWidgetPresenter) {
    if (data.completed) { //Successfully finished whole task
        this.schedulerModel.removeFromRunningQueue(sourceWidgetPresenter);
        this.schedulerModel.pushToReadyQueue(sourceWidgetPresenter);
        //get endpoint connected to the bottom of source widget
        var endpoint = this.connectionsModel.getBottomEndpointOfWidget(sourceWidgetPresenter.getWidgetId());
        if (endpoint) {
            this.unblockDependant(endpoint.toString());
        }
    } else if (data.latchCountDown) {
        this.processLatchCountDown(sourceWidgetPresenter);
    } else if (data.latchAwait) {
        this.processLatchAwait(sourceWidgetPresenter);
    } else if (data.barrierAwait) {
        this.processBarrierAwait(sourceWidgetPresenter);
    } else if (data.barrierOnZero) {
        this.processBarrierOnZero(sourceWidgetPresenter);
    } else if (data.semaphoreRelease) {
        this.processSemaphoreRelease(sourceWidgetPresenter, data.permits);
    } else if (data.semaphoreAcquire) {
        this.processSemaphoreAcquire(sourceWidgetPresenter, data.permits);
    } else if (data.phaserRegister) {
        this.processPhaserRegister(sourceWidgetPresenter);
    } else if (data.phaserArriveDeregister) {
        this.processPhaserArriveDeregister(sourceWidgetPresenter);
    } else if (data.phaserArrive) {
        this.processPhaserArrive(sourceWidgetPresenter);
    } else if (data.phaserAwaitAdvance) {
        this.processPhaserAwaitAdvance(sourceWidgetPresenter);
    } else if (data.phaserArriveAwaitAdvance) {
        this.processPhaserArriveAwaitAdvance(sourceWidgetPresenter);
    }
};

SynchEduca.Scheduler.prototype.processLatchCountDown = function (sourceWidgetPresenter) {
    console.log("Processing statement countDown latch");
    var endpoint = this.getEndpoint(sourceWidgetPresenter);
    if (endpoint) {
        var latchPresenter = this.schedulerModel.getLatchPresenter(endpoint.widgetId);
        latchPresenter.executeCountDown();
    } else {
        console.log("Uuupss", endpoint);
    }
    sourceWidgetPresenter.executeNextStatement();
};

SynchEduca.Scheduler.prototype.processLatchAwait = function (sourceWidgetPresenter) {
    console.log("Processing statement await latch");
    var endpoint = this.getEndpoint(sourceWidgetPresenter);
    if (endpoint) {
        var latchPresenter = this.schedulerModel.getLatchPresenter(endpoint.widgetId);
        if (latchPresenter.isBlocking()) {
            var idOfBlocker = endpoint.toString();
            this.schedulerModel.pushToBlockedQueue(idOfBlocker, sourceWidgetPresenter);
        } else {
            sourceWidgetPresenter.executeNextStatement();
        }
    } else {
        sourceWidgetPresenter.executeNextStatement();
    }
};

SynchEduca.Scheduler.prototype.processBarrierAwait = function (sourceWidgetPresenter) {
    console.log("Processing statement await barrrier");
    var endpoint = this.getEndpoint(sourceWidgetPresenter);
    if (endpoint) {
        var barrierPresenter = this.schedulerModel.getBarrierPresenter(endpoint.widgetId);
        if (barrierPresenter.isBlocking()) {
            var idOfBlocker = endpoint.toString();
            this.schedulerModel.pushToBlockedQueue(idOfBlocker, sourceWidgetPresenter);
            barrierPresenter.executeAwait();
        } else {
            sourceWidgetPresenter.executeNextStatement();
        }
    } else {
        sourceWidgetPresenter.executeNextStatement();
    }
};

SynchEduca.Scheduler.prototype.processBarrierOnZero = function (sourceWidgetPresenter) {
    console.log("Processing statement onZero barrier");
    var endpoint = this.getEndpoint(sourceWidgetPresenter);
    if (endpoint) {
        var barrierPresenter = this.schedulerModel.getBarrierPresenter(endpoint.widgetId);
        if (barrierPresenter.isBlocking()) {
            var idOfBlocker = endpoint.toString();
            this.schedulerModel.pushToBlockedQueue(idOfBlocker, sourceWidgetPresenter);
        } else {
            sourceWidgetPresenter.executeNextStatement();
        }
    } else {
        sourceWidgetPresenter.executeNextStatement();
    }
};

SynchEduca.Scheduler.prototype.processSemaphoreRelease = function (sourceWidgetPresenter, permits) {
    console.log("Processing statement release semaphore (" + permits + " permits)");
    var endpoint = this.getEndpoint(sourceWidgetPresenter);
    if (endpoint) {
        var semaphorePresenter = this.schedulerModel.getSemaphorePresenter(endpoint.widgetId);
        semaphorePresenter.executeRelease(permits);
    }
    sourceWidgetPresenter.executeNextStatement();
};

SynchEduca.Scheduler.prototype.processSemaphoreAcquire = function (sourceWidgetPresenter, permits) {
    console.log("Processing statement acquire semaphore (" + permits + " permits)");
    var endpoint = this.getEndpoint(sourceWidgetPresenter);
    if (endpoint) {
        var semaphorePresenter = this.schedulerModel.getSemaphorePresenter(endpoint.widgetId);
        var idOfBlocker = endpoint.toString();
        if (this.schedulerModel.peekFirstInBlockedQueue(idOfBlocker) || semaphorePresenter.isBlocking(permits)) {
            var data = {widgetPresenter: sourceWidgetPresenter, permits: permits};
            this.schedulerModel.pushToBlockedQueue(idOfBlocker, data);
        } else {
            semaphorePresenter.executeAcquire(permits);
            sourceWidgetPresenter.executeNextStatement();
        }
    } else {
        sourceWidgetPresenter.executeNextStatement();
    }
};

SynchEduca.Scheduler.prototype.processPhaserRegister = function (sourceWidgetPresenter) {
    console.log("Processing statement phaser register");
    var endpoint = this.getEndpoint(sourceWidgetPresenter);
    if (endpoint) {
        var phaserPresenter = this.schedulerModel.getPhaserPresenter(endpoint.widgetId);
        phaserPresenter.executeRegister();
    }
    sourceWidgetPresenter.executeNextStatement();
};

SynchEduca.Scheduler.prototype.processPhaserArriveDeregister = function (sourceWidgetPresenter) {
    console.log("Processing statement phaser arrive deregister");
    var endpoint = this.getEndpoint(sourceWidgetPresenter);
    if (endpoint) {
        var phaserPresenter = this.schedulerModel.getPhaserPresenter(endpoint.widgetId);
        phaserPresenter.executeArriveDeregister();
    }
    sourceWidgetPresenter.executeNextStatement();
};

SynchEduca.Scheduler.prototype.processPhaserArrive = function (sourceWidgetPresenter) {
    console.log("Processing statement phaser arrive");
    var endpoint = this.getEndpoint(sourceWidgetPresenter);
    if (endpoint) {
        var phaserPresenter = this.schedulerModel.getPhaserPresenter(endpoint.widgetId);
        phaserPresenter.executeArrive();
    }
    sourceWidgetPresenter.executeNextStatement();
};

SynchEduca.Scheduler.prototype.processPhaserAwaitAdvance = function (sourceWidgetPresenter) {
    console.log("Processing statement phaser await advance");
    var endpoint = this.getEndpoint(sourceWidgetPresenter);
    if (endpoint) {
        var phaserPresenter = this.schedulerModel.getPhaserPresenter(endpoint.widgetId);
        if (phaserPresenter.isBlocking()) {
            var idOfBlocker = endpoint.toString();
            this.schedulerModel.pushToBlockedQueue(idOfBlocker, sourceWidgetPresenter);
        } else {
            sourceWidgetPresenter.executeNextStatement();
        }
    } else {
        sourceWidgetPresenter.executeNextStatement();
    }
};

SynchEduca.Scheduler.prototype.processPhaserArriveAwaitAdvance = function (sourceWidgetPresenter) {
    console.log("Processing statement phaser arrive await advance");
    var endpoint = this.getEndpoint(sourceWidgetPresenter);
    if (endpoint) {
        var phaserPresenter = this.schedulerModel.getPhaserPresenter(endpoint.widgetId);
        if (phaserPresenter.isBlocking()) {
            var idOfBlocker = endpoint.toString();
            this.schedulerModel.pushToBlockedQueue(idOfBlocker, sourceWidgetPresenter);
            phaserPresenter.executeArriveAwaitAdvance();
        } else {
            sourceWidgetPresenter.executeNextStatement();
        }
    } else {
        sourceWidgetPresenter.executeNextStatement();
    }
};

SynchEduca.Scheduler.prototype.getEndpoint = function (sourceWidgetPresenter) {
    //Consider this statement processed (but processing is about to follow), prepare PC counter for the next statement
    var progCounter = sourceWidgetPresenter.getProgramCounter();
    sourceWidgetPresenter.incrementProgramCounter();
    return this.connectionsModel.getTargetConnectedToEndpoint(sourceWidgetPresenter.getWidgetId(), SynchEduca.ModelDashboardConnections.STATEMENT, progCounter);
};

SynchEduca.Scheduler.prototype.unblockAllDependant = function (idOfBlocker) {
    var unblockWidgetPresenter = {};
    while (unblockWidgetPresenter = this.schedulerModel.dequeueFromBlockedQueue(idOfBlocker)) {
        this.schedulerModel.pushToRunningQueue(unblockWidgetPresenter);
        unblockWidgetPresenter.executeNextStatement();
    }
};

SynchEduca.Scheduler.prototype.unblockDependant = function (idOfBlocker) {
    var widgetPresenter = this.schedulerModel.dequeueFromBlockedQueue(idOfBlocker);
    if (widgetPresenter) {
        this.schedulerModel.pushToRunningQueue(widgetPresenter);
        widgetPresenter.executeNextStatement();
    }
};

