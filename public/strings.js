var SynchEduca = SynchEduca || {};

SynchEduca.strings = {
    MSG_WANT_TO_DELETE_WIDGET: "Do you want to delete widget",
    CSS_CLASS_WIDGET_DELETE: "widget_delete",
    MSG_CONN_NOT_ALLOWED_ON_SELF: "Not allowed to create connections to itself",
    MSG_IDENTIFIER_IN_USE: "This identifier is in use, please use different one.",
    MSG_ONLY_DIGIT: "Only digits are allowed.",
    MSG_COUNT_ZERO: "Count down is already zero.",
    MSG_CHARS_NOT_ALLOWED: "Invalid characters.",
    PREVIEW_NON_SYNCH_STATEMENT: "statement",
    PREVIEW_SYNCH_STATEMENT: "await"
};