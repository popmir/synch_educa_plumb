var SynchEduca = SynchEduca || {};

//example of widgetData is defined in create_component_model.js
SynchEduca.ModelWidgetNormal = function (widgetData, callbackToPresenter) {
    this.widgetData = widgetData;
    this.registerWebWorker();
};

SynchEduca.ModelWidgetNormal.prototype.getWidgetData = function () {
    return this.widgetData;
};

SynchEduca.ModelWidgetNormal.prototype.getCurrentStatement = function () {
    return this.widgetData.statements[this.widgetData.executing_at_pc];
};

SynchEduca.ModelWidgetNormal.prototype.incrementProgramCounter = function () {
    this.widgetData.executing_at_pc++;
};

SynchEduca.ModelWidgetNormal.prototype.getProgramCounter = function () {
    return this.widgetData.executing_at_pc;
};

SynchEduca.ModelWidgetNormal.prototype.getNumberOfStatements = function () {
    return this.widgetData.statements.length;
};

SynchEduca.ModelWidgetNormal.prototype.resetProgramCounter = function () {
    this.widgetData.executing_at_pc = 0;
};

SynchEduca.ModelWidgetNormal.prototype.getCurrentParam = function () {
    return this.widgetData.params[this.widgetData.executing_at_pc];
};

SynchEduca.ModelWidgetNormal.prototype.registerWebWorker = function () {
    var _this = this;
    this.webWorker = new Worker('models/worker_normal.js');
    this.webWorker.onmessage = function (event) {
        _this.onMsgFromWorker(event.data);
    };
};

SynchEduca.ModelWidgetNormal.prototype.onMsgFromWorker = function (msg) {
    if (this._callbackToPresenter) {
        this._callbackToPresenter(msg);
    }
};

SynchEduca.ModelWidgetNormal.prototype.sendMsgToWorker = function (msg, callbackToPresenter) {
    this._callbackToPresenter = callbackToPresenter;
    this.webWorker.postMessage(msg);
};

SynchEduca.ModelWidgetNormal.prototype.resetWorker = function () {
    this._callbackToPresenter = null;
    this.webWorker.terminate();
    this.registerWebWorker();
};

