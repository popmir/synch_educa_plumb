var SynchEduca = SynchEduca || {};

SynchEduca.ModelDashboardConnections = function () {
    this.connections = {}; // //connectionId is key, value is our Connection object
    this.endpoints = {}; //endpointId is key, value is our Endpoint object
};

SynchEduca.ModelDashboardConnections.TOP = "top";
SynchEduca.ModelDashboardConnections.BOTTOM = "bottom";
SynchEduca.ModelDashboardConnections.STATEMENT = "statement";
SynchEduca.ModelDashboardConnections.LATCH_AWAIT = "latch_await";
SynchEduca.ModelDashboardConnections.LATCH_COUNT_DOWN = "latch_count_down";
SynchEduca.ModelDashboardConnections.BARRIER_AWAIT = "barrier_await";
SynchEduca.ModelDashboardConnections.BARRIER_ON_ZERO = "barrier_on_zero";
SynchEduca.ModelDashboardConnections.SEMAPHORE_ACQUIRE = "semaphore_acquire";
SynchEduca.ModelDashboardConnections.SEMAPHORE_RELEASE = "semaphore_release";
SynchEduca.ModelDashboardConnections.PHASER_REGISTER = "phaser_register";
SynchEduca.ModelDashboardConnections.PHASER_ARRIVE_DEREGISTER = "phaser_arrive_deregister";
SynchEduca.ModelDashboardConnections.PHASER_ARRIVE = "phaser_arrive";
SynchEduca.ModelDashboardConnections.PHASER_AWAIT_ADVANCE = "phaser_await_advance";
SynchEduca.ModelDashboardConnections.PHASER_ARRIVE_AWAIT_ADVANCE = "phaser_arrive_await_advance";

SynchEduca.ModelDashboardConnections.prototype.addConnection = function (connection) {
    this.connections[connection.getId()] = new SynchEduca.Connection(
        this.endpoints[connection.endpoints[0].getId()],
        this.endpoints[connection.endpoints[1].getId()], connection
    );
};

SynchEduca.ModelDashboardConnections.prototype.addTopEndpoint = function (endpointId, widgetId, jsPlumbEndpoint) {
    return this.endpoints[endpointId] = new SynchEduca.Endpoint(widgetId, SynchEduca.ModelDashboardConnections.TOP, undefined, jsPlumbEndpoint);
};

SynchEduca.ModelDashboardConnections.prototype.addBottomEndpoint = function (endpointId, widgetId, jsPlumbEndpoint) {
    return this.endpoints[endpointId] = new SynchEduca.Endpoint(widgetId, SynchEduca.ModelDashboardConnections.BOTTOM, undefined, jsPlumbEndpoint);
};

SynchEduca.ModelDashboardConnections.prototype.addStatementEndpoint = function (endpointId, widgetId, positionId, jsPlumbEndpoint) {
    return this.endpoints[endpointId] = new SynchEduca.Endpoint(widgetId, SynchEduca.ModelDashboardConnections.STATEMENT, positionId, jsPlumbEndpoint);
};

SynchEduca.ModelDashboardConnections.prototype.addLatchAwaitEndpoint = function (endpointId, widgetId, jsPlumbEndpoint) {
    return this.endpoints[endpointId] = new SynchEduca.Endpoint(widgetId, SynchEduca.ModelDashboardConnections.LATCH_AWAIT, undefined, jsPlumbEndpoint);
};

SynchEduca.ModelDashboardConnections.prototype.addLatchCountDownEndpoint = function (endpointId, widgetId, jsPlumbEndpoint) {
    return this.endpoints[endpointId] = new SynchEduca.Endpoint(widgetId, SynchEduca.ModelDashboardConnections.LATCH_COUNT_DOWN, undefined, jsPlumbEndpoint);
};

SynchEduca.ModelDashboardConnections.prototype.addBarrierAwaitEndpoint = function (endpointId, widgetId, jsPlumbEndpoint) {
    return this.endpoints[endpointId] = new SynchEduca.Endpoint(widgetId, SynchEduca.ModelDashboardConnections.BARRIER_AWAIT, undefined, jsPlumbEndpoint);
};

SynchEduca.ModelDashboardConnections.prototype.addBarrierOnZeroEndpoint = function (endpointId, widgetId, jsPlumbEndpoint) {
    return this.endpoints[endpointId] = new SynchEduca.Endpoint(widgetId, SynchEduca.ModelDashboardConnections.BARRIER_ON_ZERO, undefined, jsPlumbEndpoint);
};

SynchEduca.ModelDashboardConnections.prototype.addSemaphoreAcquireEndpoint = function (endpointId, widgetId, jsPlumbEndpoint) {
    return this.endpoints[endpointId] = new SynchEduca.Endpoint(widgetId, SynchEduca.ModelDashboardConnections.SEMAPHORE_ACQUIRE, undefined, jsPlumbEndpoint);
};

SynchEduca.ModelDashboardConnections.prototype.addSemaphoreReleaseEndpoint = function (endpointId, widgetId, jsPlumbEndpoint) {
    return this.endpoints[endpointId] = new SynchEduca.Endpoint(widgetId, SynchEduca.ModelDashboardConnections.SEMAPHORE_RELEASE, undefined, jsPlumbEndpoint);
};

SynchEduca.ModelDashboardConnections.prototype.addPhaserRegisterEndpoint = function (endpointId, widgetId, jsPlumbEndpoint) {
    return this.endpoints[endpointId] = new SynchEduca.Endpoint(widgetId, SynchEduca.ModelDashboardConnections.PHASER_REGISTER, undefined, jsPlumbEndpoint);
};

SynchEduca.ModelDashboardConnections.prototype.addPhaserArriveDeregisterEndpoint = function (endpointId, widgetId, jsPlumbEndpoint) {
    return this.endpoints[endpointId] = new SynchEduca.Endpoint(widgetId, SynchEduca.ModelDashboardConnections.PHASER_ARRIVE_DEREGISTER, undefined, jsPlumbEndpoint);
};

SynchEduca.ModelDashboardConnections.prototype.addPhaserArriveEndpoint = function (endpointId, widgetId, jsPlumbEndpoint) {
    return this.endpoints[endpointId] = new SynchEduca.Endpoint(widgetId, SynchEduca.ModelDashboardConnections.PHASER_ARRIVE, undefined, jsPlumbEndpoint);
};

SynchEduca.ModelDashboardConnections.prototype.addPhaserAwaitAdvanceEndpoint = function (endpointId, widgetId, jsPlumbEndpoint) {
    return this.endpoints[endpointId] = new SynchEduca.Endpoint(widgetId, SynchEduca.ModelDashboardConnections.PHASER_AWAIT_ADVANCE, undefined, jsPlumbEndpoint);
};

SynchEduca.ModelDashboardConnections.prototype.addPhaserArriveAwaitAdvanceEndpoint = function (endpointId, widgetId, jsPlumbEndpoint) {
    return this.endpoints[endpointId] = new SynchEduca.Endpoint(widgetId, SynchEduca.ModelDashboardConnections.PHASER_ARRIVE_AWAIT_ADVANCE, undefined, jsPlumbEndpoint);
};

//PositionId must be undefined if typeId is TOP or BOTTOM
SynchEduca.ModelDashboardConnections.prototype.getSourceConnectedToEndpoint = function (widgetId, typeId, positionId) {
    var sourceEndpoint = null;
    for(var connectionKey in this.connections) {
      var connection = this.connections[connectionKey];
      if (connection.targetEndpoint.widgetId === widgetId
          && connection.targetEndpoint.typeId === typeId
          && connection.targetEndpoint.positionId === positionId) {
          sourceEndpoint = connection.sourceEndpoint;
      }
    }
    return sourceEndpoint;
};

//TODO: make async
SynchEduca.ModelDashboardConnections.prototype.getTargetConnectedToEndpoint = function (widgetId, typeId, positionId) {
    var targetEndpoint = null;
    for(var connectionKey in this.connections) {
      var connection = this.connections[connectionKey];
      if (connection.sourceEndpoint.widgetId === widgetId
          && connection.sourceEndpoint.typeId === typeId
          && connection.sourceEndpoint.positionId === positionId) {
          targetEndpoint = connection.targetEndpoint;
      }
    }
    return targetEndpoint;
};

SynchEduca.ModelDashboardConnections.prototype.getBottomEndpointOfWidget = function (widgetId) {
    var endpoint = null;
    for(var connectionKey in this.connections) {
      var connection = this.connections[connectionKey];
      if (connection.sourceEndpoint.widgetId === widgetId
          && connection.sourceEndpoint.typeId === SynchEduca.ModelDashboardConnections.BOTTOM) {
          endpoint = connection.sourceEndpoint;
      }
    }
    return endpoint;
};

SynchEduca.ModelDashboardConnections.prototype.removeAll = function (widgetId) {    
    widgetId += '#'; //To ensure widget containing widgetId as prefix will not match criteria
    for (var connectionKey in this.connections){
        if (this.connections[connectionKey].toString().indexOf(widgetId) >= 0) {
            if (this.connections[connectionKey].sourceEndpoint.toString().indexOf(widgetId) >= 0) {
                delete this.endpoints[this.connections[connectionKey].sourceEndpoint.jsPlumbEndpoint.getId()];
            } else if (this.connections[connectionKey].targetEndpoint.toString().indexOf(widgetId) >= 0) {
                delete this.endpoints[this.connections[connectionKey].targetEndpoint.jsPlumbEndpoint.getId()];
            }
            delete this.connections[connectionKey];
        }
    };

    for (var endpointKey in this.endpoints) {
        if (this.endpoints[endpointKey].toString().indexOf(widgetId) >= 0) {
            delete this.endpoints[endpointKey];
        }
    };
};

SynchEduca.ModelDashboardConnections.prototype.checkIfExists = function (endpointId) {    
    return (endpointId in this.endpoints);
}

// Our Endpoint class that decorated jsPlumb endpoints 
// PositionId must be defined only if typeId is STATEMENT, all other cases it is 
// undefined as for typeId is TOP or BOTTOM, LATCH_AWAIT, etc
SynchEduca.Endpoint = function (widgetId, typeId, positionId, jsPlumbEndpoint) {
    this.widgetId = widgetId;
    this.typeId = typeId;
    this.positionId = positionId;
    this.jsPlumbEndpoint = jsPlumbEndpoint;
};

SynchEduca.Endpoint.prototype.toString = function () {
    return this.widgetId + '#' + this.typeId + "~" + this.positionId;
};

SynchEduca.Connection = function (sourceEndpoint, targetEndpoint, jsPlumbConnection) {
    this.sourceEndpoint = sourceEndpoint;
    this.targetEndpoint = targetEndpoint;
    this.jsPlumbConnection = jsPlumbConnection;
};

SynchEduca.Connection.prototype.toString = function () {
    return this.sourceEndpoint.toString() + '@' + this.targetEndpoint.toString();
};