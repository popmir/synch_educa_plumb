var SynchEduca = SynchEduca || {};

self.onmessage = function (event) {
    var resp = {
        success: true
    };
    if (event.data === "executeNext") {
        setTimeout(function () {
            self.postMessage(resp);
        }, 1000);
    } else if (event.data === "delete") {
        self.postMessage('\nMsg from worker:\n    I have received msg:' + event.data);
        setTimeout(function () {
            self.postMessage('\nI have been sleeping');
        }, 3000);
    }
};
