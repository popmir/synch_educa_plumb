var SynchEduca = SynchEduca || {};

//example of widgetData is defined in create_component_model.js
SynchEduca.ModelLatchWidget = function (widgetData, callbackToPresenter) {
    this.widgetData = widgetData;
    this.widgetData.countDownValue = this.widgetData.initValue;
};

SynchEduca.ModelLatchWidget.prototype.getWidgetData = function () {
    return this.widgetData;
};

SynchEduca.ModelLatchWidget.prototype.getCurrentCount = function () {
    return this.widgetData.countDownValue;
};

SynchEduca.ModelLatchWidget.prototype.decrementCount = function () {
    return --this.widgetData.countDownValue;
};

//For Latch this is triggered on Start execution
SynchEduca.ModelLatchWidget.prototype.resetCount = function () {
    this.widgetData.countDownValue = this.widgetData.initValue;
};
