var SynchEduca = SynchEduca || {};

//example of widgetData is defined in create_component_model.js
SynchEduca.ModelSemaphoreWidget = function (widgetData, callbackToPresenter) {
    this.widgetData = widgetData;
    this.widgetData.permitValue = this.widgetData.initValue;
};

SynchEduca.ModelSemaphoreWidget.prototype.getWidgetData = function () {
    return this.widgetData;
};

SynchEduca.ModelSemaphoreWidget.prototype.getCurrentCount = function () {
    return this.widgetData.permitValue;
};

SynchEduca.ModelSemaphoreWidget.prototype.addPermits = function (permits) {
    this.widgetData.permitValue = +this.widgetData.permitValue + parseInt(permits);
    return this.widgetData.permitValue;
};

SynchEduca.ModelSemaphoreWidget.prototype.removePermits = function (permits) {
    this.widgetData.permitValue = +this.widgetData.permitValue - parseInt(permits);
    return this.widgetData.permitValue;
};

//For Semaphore this is triggered on Start execution
SynchEduca.ModelSemaphoreWidget.prototype.resetCount = function () {
    this.widgetData.permitValue = this.widgetData.initValue;
};
