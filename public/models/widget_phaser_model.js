var SynchEduca = SynchEduca || {};

//example of widgetData is defined in create_component_model.js
SynchEduca.ModelPhaserWidget = function (widgetData, callbackToPresenter) {
    this.widgetData = widgetData;
    this.widgetData.registeredValue = this.widgetData.initValue;
    this.widgetData.unarrivedValue = this.widgetData.initValue;
};

SynchEduca.ModelPhaserWidget.prototype.getWidgetData = function () {
    return this.widgetData;
};

SynchEduca.ModelPhaserWidget.prototype.getCurrentRegisteredCount = function () {
    return this.widgetData.registeredValue;
};

SynchEduca.ModelPhaserWidget.prototype.getCurrentUnarrivedCount = function () {
    return this.widgetData.unarrivedValue;
};

SynchEduca.ModelPhaserWidget.prototype.incrementRegisteredCount = function () {
    return ++this.widgetData.registeredValue;
};

SynchEduca.ModelPhaserWidget.prototype.incrementUnarrivedCount = function () {
    return ++this.widgetData.unarrivedValue;
};

SynchEduca.ModelPhaserWidget.prototype.decrementRegisteredCount = function () {
    return --this.widgetData.registeredValue;
};

SynchEduca.ModelPhaserWidget.prototype.decrementUnarrivedCount = function () {
    return --this.widgetData.unarrivedValue;
};

//For Phaser this is triggered on Start execution
SynchEduca.ModelPhaserWidget.prototype.resetCount = function () {
    this.widgetData.registeredValue = this.widgetData.initValue;
    this.widgetData.unarrivedValue = this.widgetData.initValue;
};

SynchEduca.ModelPhaserWidget.prototype.resetExecutionCount = function () {
    this.widgetData.unarrivedValue = this.widgetData.registeredValue
};
