var SynchEduca = SynchEduca || {};

SynchEduca.ModelScheduler = function () {
    this._mapWidgetPresenters = {};
    this._mapLatchPresenters = {};
    this._mapBarrierPresenters = {};
    this._mapSemaphorePresenters = {};
    this._mapPhaserPresenters = {};
    this._queueReady = [];
    this._queueRunning = [];
    this._blockedQueues = {}; // has queue for each id of the blocker
};

//Adds widget to dictionary
SynchEduca.ModelScheduler.prototype.registerWidget = function (widgetId, widgetPresenter) {
    this._mapWidgetPresenters[widgetId] = widgetPresenter;
    this._queueReady.push(widgetPresenter);
};

SynchEduca.ModelScheduler.prototype.unregisterWidget = function (widgetId) {
    delete this._mapWidgetPresenters[widgetId];
};

SynchEduca.ModelScheduler.prototype.getWidgetPresenter = function (widgetId) {
    return this._mapWidgetPresenters[widgetId];
};

//Adds latch mechanism to dictionary
SynchEduca.ModelScheduler.prototype.registerLatch = function (widgetId, widgetPresenter) {
    this._mapLatchPresenters[widgetId] = widgetPresenter;
};

SynchEduca.ModelScheduler.prototype.unregisterLatch = function (widgetId) {
    delete this._mapLatchPresenters[widgetId];
};

SynchEduca.ModelScheduler.prototype.getLatchPresenter = function (widgetId) {
    return this._mapLatchPresenters[widgetId];
};

//Adds barrier mechanism to dictionary
SynchEduca.ModelScheduler.prototype.registerBarrier = function (widgetId, widgetPresenter) {
    this._mapBarrierPresenters[widgetId] = widgetPresenter;
};

SynchEduca.ModelScheduler.prototype.unregisterBarrier = function (widgetId) {
    delete this._mapBarrierPresenters[widgetId];
};

SynchEduca.ModelScheduler.prototype.getBarrierPresenter = function (widgetId) {
    return this._mapBarrierPresenters[widgetId];
};

//Adds semaphore mechanism to dictionary
SynchEduca.ModelScheduler.prototype.registerSemaphore = function (widgetId, widgetPresenter) {
    this._mapSemaphorePresenters[widgetId] = widgetPresenter;
};

SynchEduca.ModelScheduler.prototype.unregisterSemaphore = function (widgetId) {
    delete this._mapSemaphorePresenters[widgetId];
};

SynchEduca.ModelScheduler.prototype.getSemaphorePresenter = function (widgetId) {
    return this._mapSemaphorePresenters[widgetId];
};

//Adds phaser mechanism to dictionary
SynchEduca.ModelScheduler.prototype.registerPhaser = function (widgetId, widgetPresenter) {
    this._mapPhaserPresenters[widgetId] = widgetPresenter;
};

SynchEduca.ModelScheduler.prototype.unregisterPhaser = function (widgetId) {
    delete this._mapPhaserPresenters[widgetId];
};

SynchEduca.ModelScheduler.prototype.getPhaserPresenter = function (widgetId) {
    return this._mapPhaserPresenters[widgetId];
};

//Perform callback function on all widgets
SynchEduca.ModelScheduler.prototype.iterateAllWidgets = function (callback) {
    var _this = this;
    Object.keys(this._mapWidgetPresenters).forEach(function (widgetIdKey) {
            var widgetPresenter = _this._mapWidgetPresenters[widgetIdKey];
            callback(widgetPresenter);
        }
    );
};

//Perform callback function on all synchronization mechanisms
SynchEduca.ModelScheduler.prototype.iterateAllMechanisms = function (callback) {
    var _this = this;
    Object.keys(this._mapLatchPresenters).forEach(function (widgetIdKey) {
            var widgetPresenter = _this._mapLatchPresenters[widgetIdKey];
            callback(widgetPresenter);
        }
    );
    Object.keys(this._mapBarrierPresenters).forEach(function (widgetIdKey) {
            var widgetPresenter = _this._mapBarrierPresenters[widgetIdKey];
            callback(widgetPresenter);
        }
    );
    Object.keys(this._mapSemaphorePresenters).forEach(function (widgetIdKey) {
            var widgetPresenter = _this._mapSemaphorePresenters[widgetIdKey];
            callback(widgetPresenter);
        }
    );
    Object.keys(this._mapPhaserPresenters).forEach(function (widgetIdKey) {
            var widgetPresenter = _this._mapPhaserPresenters[widgetIdKey];
            callback(widgetPresenter);
        }
    );
};

SynchEduca.ModelScheduler.prototype.resetQueues = function () {
    this._queueReady = [];
    this._queueRunning = [];
    this._blockedQueues = {};
};

//For suspended queue
SynchEduca.ModelScheduler.prototype.pushToReadyQueue = function (widgetPresenter) {
    this._queueReady.push(widgetPresenter);
};
SynchEduca.ModelScheduler.prototype.peekLastInReadyQueue = function () {
    return this._queueReady[this._queueReady.length - 1];
};
SynchEduca.ModelScheduler.prototype.peekFirstInReadyQueue = function () {
    return this._queueReady[0];
};
SynchEduca.ModelScheduler.prototype.popFromReadyQueue = function () {
    return this._queueReady.pop();
};
SynchEduca.ModelScheduler.prototype.dequeueFromReadyQueue = function () {
    return this._queueReady.shift();
};
SynchEduca.ModelScheduler.prototype.removeFromReadyQueue = function (widgetPresenter) {
    return SynchEduca.Utils.findAndRemoveFromArray(this._queueReady, widgetPresenter);
};

//For active queue
SynchEduca.ModelScheduler.prototype.pushToRunningQueue = function (widgetPresenter) {
    this._queueRunning.push(widgetPresenter);
};
SynchEduca.ModelScheduler.prototype.peekLastInRunningQueue = function () {
    return this._queueRunning[this._queueReady.length - 1];
};
SynchEduca.ModelScheduler.prototype.peekFirstInRunningQueue = function () {
    return this._queueRunning[0];
};
SynchEduca.ModelScheduler.prototype.popFromRunningQueue = function () {
    return this._queueRunning.pop();
};
SynchEduca.ModelScheduler.prototype.dequeueFromRunningQueue = function () {
    return this._queueRunning.shift();
};
SynchEduca.ModelScheduler.prototype.removeFromRunningQueue = function (widgetPresenter) {
    return SynchEduca.Utils.findAndRemoveFromArray(this._queueRunning, widgetPresenter);
};

//For blocked queues
SynchEduca.ModelScheduler.prototype.pushToBlockedQueue = function (idOfBlocker, blockedPresenter) {
    this._blockedQueues[idOfBlocker] = this._blockedQueues[idOfBlocker] || [];
    this._blockedQueues[idOfBlocker].push(blockedPresenter);
};
SynchEduca.ModelScheduler.prototype.peekLastInBlockedQueue = function (idOfBlocker) {
    if (idOfBlocker in this._blockedQueues) {
        return this._blockedQueues[idOfBlocker][this._blockedQueues[idOfBlocker].length - 1];
    } else {
        return undefined;
    }
};
SynchEduca.ModelScheduler.prototype.peekFirstInBlockedQueue = function (idOfBlocker) {
    if (idOfBlocker in this._blockedQueues) {
        return this._blockedQueues[idOfBlocker][0];
    } else {
        return undefined;
    }
};
SynchEduca.ModelScheduler.prototype.popFromBlockedQueue = function (idOfBlocker) {
    if (idOfBlocker in this._blockedQueues) {
        return this._blockedQueues[idOfBlocker].pop();
    }
};
SynchEduca.ModelScheduler.prototype.dequeueFromBlockedQueue = function (idOfBlocker) {
    if (idOfBlocker in this._blockedQueues) {
        return this._blockedQueues[idOfBlocker].shift();
    }
};
SynchEduca.ModelScheduler.prototype.removeFromBlockedQueue = function (idOfBlocker, widgetPresenter) {
    if (idOfBlocker in this._blockedQueues) {
        if (widgetPresenter.widgetPresenter) { // maybe not needed
            return SynchEduca.Utils.findAndRemoveFromArray(this._blockedQueues[idOfBlocker], widgetPresenter.widgetPresenter);
        }
        return SynchEduca.Utils.findAndRemoveFromArray(this._blockedQueues[idOfBlocker], widgetPresenter);
    }
};

SynchEduca.ModelScheduler.prototype.removeFromAllBlockedQueues = function (widgetPresenter) {
    var _this = this;
    Object.keys(this._blockedQueues).forEach(function (idOfBlocker) {
            _this.removeFromBlockedQueue(idOfBlocker, widgetPresenter);
        }
    );
};