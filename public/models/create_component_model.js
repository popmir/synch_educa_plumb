var SynchEduca = SynchEduca || {};

SynchEduca.ModelCreateComponent = function () {
    this.countLatchWidgets = 1;
    this.countBarrierWidgets = 1;
    this.countSemaphoreWidgets = 1;
    this.countPhaserWidgets = 1;
    this.countPreviewStatements = 1;
    this.previewNormalWidgetData = {
        executing_at_pc: 0,
        statements: [],
        //each statement in array of statements has its type at corresponding index
        statementsType: [],
        params: []
    };

    this.previewLatchData = {
        name: "CountDownLatch", //This name is static, not editable by user
        initValue: 3 //countDown value will be set to this initValue
    };

    this.previewBarrierData = {
        name: "CyclicBarrier", //This name is static, not editable by user
        initValue: 3 //cycle value will be set to this initValue
    };

    this.previewSemaphoreData = {
        name: "Semaphore", //This name is static, not editable by user
        initValue: 1 //permits value will be set to this initValue
    };

    this.previewPhaserData = {
        name: "Phaser", //This name is static, not editable by user
        initValue: 0 //permits value will be set to this initValue
    };
};

SynchEduca.ModelCreateComponent.STATEMENT_PLAIN = "statement";
SynchEduca.ModelCreateComponent.STATEMENT_AWAIT_LATCH = "await latch";
SynchEduca.ModelCreateComponent.STATEMENT_COUNTDOWN_LATCH = "countDown latch";
SynchEduca.ModelCreateComponent.STATEMENT_AWAIT_BARRIER = "await barrier";
SynchEduca.ModelCreateComponent.STATEMENT_ONZERO_BARRIER = "onZero barrier";
SynchEduca.ModelCreateComponent.STATEMENT_ACQUIRE_SEMAPHORE = "acquire semaphore";
SynchEduca.ModelCreateComponent.STATEMENT_RELEASE_SEMAPHORE = "release semaphore";
SynchEduca.ModelCreateComponent.STATEMENT_REGISTER_PHASER = "register phaser";
SynchEduca.ModelCreateComponent.STATEMENT_ARRIVE_DEREGISTER_PHASER = "arrive and deregister phaser";
SynchEduca.ModelCreateComponent.STATEMENT_ARRIVE_PHASER = "arrive phaser";
SynchEduca.ModelCreateComponent.STATEMENT_AWAIT_ADVANCE_PHASER = "await advance phaser";
SynchEduca.ModelCreateComponent.STATEMENT_ARRIVE_AWAIT_ADVANCE_PHASER = "arrive await advance phaser";
SynchEduca.ModelCreateComponent.STATEMENT_TYPE_SYNCH = "synch";
SynchEduca.ModelCreateComponent.STATEMENT_TYPE_NORMAL = "normal";

SynchEduca.ModelCreateComponent.prototype.getExampleWorkerWidgetData = function () {
    return this.cloneData(this.previewNormalWidgetData);
};

//Deep copy
SynchEduca.ModelCreateComponent.prototype.cloneData = function (srcData) {
    return jQuery.extend(true, {}, srcData);
};

SynchEduca.ModelCreateComponent.prototype.setName = function (name) {
    this.previewNormalWidgetData.name = name;
};

SynchEduca.ModelCreateComponent.prototype.addPlainStatement = function () {
    var caption = this.countPreviewStatements++ + ". " + SynchEduca.ModelCreateComponent.STATEMENT_PLAIN;
    this.previewNormalWidgetData.params.push(null);
    this.previewNormalWidgetData.statements.push(caption);
    this.previewNormalWidgetData.statementsType.push(SynchEduca.ModelCreateComponent.STATEMENT_TYPE_NORMAL);
    return caption;
};

SynchEduca.ModelCreateComponent.prototype.addSynchStatement = function (statement) {
    var caption = this.countPreviewStatements++ + ". " + statement;
    this.previewNormalWidgetData.params.push(null);
    this.previewNormalWidgetData.statements.push(caption);
    this.previewNormalWidgetData.statementsType.push(SynchEduca.ModelCreateComponent.STATEMENT_TYPE_SYNCH);
    return caption;
};

SynchEduca.ModelCreateComponent.prototype.addSynchStatementExtended = function (statement, param) {
    var prefix = " (" + param + ")";
    var caption = this.countPreviewStatements++ + ". " + statement + prefix;
    this.previewNormalWidgetData.params.push(param);
    this.previewNormalWidgetData.statements.push(caption);
    this.previewNormalWidgetData.statementsType.push(SynchEduca.ModelCreateComponent.STATEMENT_TYPE_SYNCH);
    return caption;
};

SynchEduca.ModelCreateComponent.prototype.removeLastStatement = function () {
    if (this.countPreviewStatements > 1) {
        this.previewNormalWidgetData.params.pop();
        this.previewNormalWidgetData.statements.pop();
        this.previewNormalWidgetData.statementsType.pop();
        this.countPreviewStatements--;
    }
};

SynchEduca.ModelCreateComponent.prototype.clearAllStatements = function () {
    this.previewNormalWidgetData.params = [];
    this.previewNormalWidgetData.statements = [];
    this.previewNormalWidgetData.statementsType = [];
    this.countPreviewStatements = 1;
};

//Latch widget handling
SynchEduca.ModelCreateComponent.prototype.setLatchInitialValue = function (initValue) {
    this.previewLatchData.initValue = initValue;
};

SynchEduca.ModelCreateComponent.prototype.incrementLatchNameSuffix = function () {
    return ++this.countLatchWidgets;
};

SynchEduca.ModelCreateComponent.prototype.getPreviewLatchData = function () {
    var latchData = this.cloneData(this.previewLatchData);
    latchData.name += this.countLatchWidgets; //name will not be editable by the user
    return latchData;
};

//Barrier widget handling
SynchEduca.ModelCreateComponent.prototype.setBarrierInitialValue = function (initValue) {
    this.previewBarrierData.initValue = initValue;
};

SynchEduca.ModelCreateComponent.prototype.incrementBarrierNameSuffix = function () {
    return ++this.countBarrierWidgets;
};

SynchEduca.ModelCreateComponent.prototype.getPreviewBarrierData = function () {
    var barrierData = this.cloneData(this.previewBarrierData);
    barrierData.name += this.countBarrierWidgets; //name will not be editable by the user
    return barrierData;
};

//Semaphore widget handling
SynchEduca.ModelCreateComponent.prototype.setSemaphoreInitialValue = function (initValue) {
    this.previewSemaphoreData.initValue = initValue;
};

SynchEduca.ModelCreateComponent.prototype.incrementSemaphoreNameSuffix = function () {
    return ++this.countSemaphoreWidgets;
};

SynchEduca.ModelCreateComponent.prototype.getPreviewSemaphoreData = function () {
    var semaphoreData = this.cloneData(this.previewSemaphoreData);
    semaphoreData.name += this.countSemaphoreWidgets; //name will not be editable by the user
    return semaphoreData;
};

//Phaser widget handling
SynchEduca.ModelCreateComponent.prototype.setPhaserInitialValue = function (initValue) {
    this.previewPhaserData.initValue = initValue;
};

SynchEduca.ModelCreateComponent.prototype.incrementPhaserNameSuffix = function () {
    return ++this.countPhaserWidgets;
};

SynchEduca.ModelCreateComponent.prototype.getPreviewPhaserData = function () {
    var phaserData = this.cloneData(this.previewPhaserData);
    phaserData.name += this.countPhaserWidgets; //name will not be editable by the user
    return phaserData;
};