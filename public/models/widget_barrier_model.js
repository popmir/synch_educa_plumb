var SynchEduca = SynchEduca || {};

//example of widgetData is defined in create_component_model.js
SynchEduca.ModelBarrierWidget = function (widgetData, callbackToPresenter) {
    this.widgetData = widgetData;
    this.widgetData.cycleValue = this.widgetData.initValue;
};

SynchEduca.ModelBarrierWidget.prototype.getWidgetData = function () {
    return this.widgetData;
};

SynchEduca.ModelBarrierWidget.prototype.getCurrentCount = function () {
    return this.widgetData.cycleValue;
};

SynchEduca.ModelBarrierWidget.prototype.decrementCount = function () {
    return --this.widgetData.cycleValue;
};

//For Barrier this is triggered on Start execution and when cycle value is 0
SynchEduca.ModelBarrierWidget.prototype.resetCount = function () {
    this.widgetData.cycleValue = this.widgetData.initValue;
};
