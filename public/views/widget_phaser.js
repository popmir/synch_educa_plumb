var SynchEduca = SynchEduca || {};

// name maps to id of the div containing the widget UI
SynchEduca.ViewPhaserWidget = function (parentContainer, widgetData) {
    this.widgetData = widgetData;
    this.parentContainer = parentContainer;
    this.presenter = new SynchEduca.PresenterPhaserWidget(this, widgetData);
};

SynchEduca.ViewPhaserWidget.SCOPE_REGISTER = "phaser:register";
SynchEduca.ViewPhaserWidget.SCOPE_ARRIVE_DEREGISTER = "phaser:arrive_deregister";
SynchEduca.ViewPhaserWidget.SCOPE_ARRIVE = "phaser:arrive";
SynchEduca.ViewPhaserWidget.SCOPE_AWAIT_ADVANCE = "phaser:await_advance";
SynchEduca.ViewPhaserWidget.SCOPE_ARRIVE_AWAIT_ADVANCE = "phaser:arrive_await_advance";

SynchEduca.ViewPhaserWidget.MAX_REGISTER_CONNECTIONS = 100;
SynchEduca.ViewPhaserWidget.MAX_ARRIVE_DEREGISTER_CONNECTIONS = 100;
SynchEduca.ViewPhaserWidget.MAX_ARRIVE_CONNECTIONS = 100;
SynchEduca.ViewPhaserWidget.MAX_AWAIT_ADVANCE_CONNECTIONS = 100;
SynchEduca.ViewPhaserWidget.MAX_ARRIVE_AWAIT_ADVANCE_CONNECTIONS = 100;

// Singleton endpoint options - same for all widgets
SynchEduca.ViewPhaserWidget.sourceRegisterEndpointOptions = {
    isSource: true,
    isTarget: false,
    endpoint: ["Dot", {radius: 10}],
    paintStyle: {
        fill: "#ffe900",
        radius: 7
    },
    connector: ["Bezier", {curveness: 175}],
    connectorStyle: {strokeWidth: 3, stroke: '#ffe900'},
    scope: SynchEduca.ViewPhaserWidget.SCOPE_REGISTER
};

SynchEduca.ViewPhaserWidget.sourceArriveDeregisterEndpointOptions = {
    isSource: true,
    isTarget: false,
    endpoint: ["Dot", {radius: 10}],
    paintStyle: {
        fill: "#ffe900",
        radius: 7
    },
    connector: ["Bezier", {curveness: 175}],
    connectorStyle: {strokeWidth: 3, stroke: '#ffe900'},
    scope: SynchEduca.ViewPhaserWidget.SCOPE_ARRIVE_DEREGISTER
};

SynchEduca.ViewPhaserWidget.sourceArriveEndpointOptions = {
    isSource: true,
    isTarget: false,
    endpoint: ["Dot", {radius: 10}],
    paintStyle: {
        fill: "#ffe900",
        radius: 7
    },
    connector: ["Bezier", {curveness: 175}],
    connectorStyle: {strokeWidth: 3, stroke: '#ffe900'},
    scope: SynchEduca.ViewPhaserWidget.SCOPE_ARRIVE
};

SynchEduca.ViewPhaserWidget.sourceAwaitAdvanceEndpointOptions = {
    isSource: true,
    isTarget: false,
    endpoint: ["Dot", {radius: 10}],
    paintStyle: {
        fill: "#ffe900",
        radius: 7
    },
    connector: ["Bezier", {curveness: 175}],
    connectorStyle: {strokeWidth: 3, stroke: '#ffe900'},
    scope: SynchEduca.ViewPhaserWidget.SCOPE_AWAIT_ADVANCE
};

SynchEduca.ViewPhaserWidget.sourceArriveAwaitAdvanceEndpointOptions = {
    isSource: true,
    isTarget: false,
    endpoint: ["Dot", {radius: 10}],
    paintStyle: {
        fill: "#ffe900",
        radius: 7
    },
    connector: ["Bezier", {curveness: 175}],
    connectorStyle: {strokeWidth: 3, stroke: '#ffe900'},
    scope: SynchEduca.ViewPhaserWidget.SCOPE_ARRIVE_AWAIT_ADVANCE
};

SynchEduca.ViewPhaserWidget.targetRegisterEndpointOptions = {
    isSource: false,
    isTarget: true,
    endpoint: ["Dot", {radius: 10}],
    paintStyle: {
        stroke: "#ffe900",
        fill: "transparent",
        radius: 7,
        strokeWidth: 2
    },
    connector: ["Bezier", {curveness: 175}],
    connectorStyle: {strokeWidth: 3, stroke: '#ffe900'},
    maxConnections: SynchEduca.ViewPhaserWidget.MAX_REGISTER_CONNECTIONS,
    onMaxConnections: function (info, e) {
        alert("Maximum connections (" + info.maxConnections + ") reached");
    },
    scope: SynchEduca.ViewPhaserWidget.SCOPE_REGISTER
};

SynchEduca.ViewPhaserWidget.targetArriveDeregisterEndpointOptions = {
    isSource: false,
    isTarget: true,
    endpoint: ["Dot", {radius: 10}],
    paintStyle: {
        stroke: "#ffe900",
        fill: "transparent",
        radius: 7,
        strokeWidth: 2
    },
    connector: ["Bezier", {curveness: 175}],
    connectorStyle: {strokeWidth: 3, stroke: '#ffe900'},
    maxConnections: SynchEduca.ViewPhaserWidget.MAX_ARRIVE_DEREGISTER_CONNECTIONS,
    onMaxConnections: function (info, e) {
        alert("Maximum connections (" + info.maxConnections + ") reached");
    },
    scope: SynchEduca.ViewPhaserWidget.SCOPE_ARRIVE_DEREGISTER
};

SynchEduca.ViewPhaserWidget.targetArriveEndpointOptions = {
    isSource: false,
    isTarget: true,
    endpoint: ["Dot", {radius: 10}],
    paintStyle: {
        stroke: "#ffe900",
        fill: "transparent",
        radius: 7,
        strokeWidth: 2
    },
    connector: ["Bezier", {curveness: 175}],
    connectorStyle: {strokeWidth: 3, stroke: '#ffe900'},
    maxConnections: SynchEduca.ViewPhaserWidget.MAX_ARRIVE_CONNECTIONS,
    onMaxConnections: function (info, e) {
        alert("Maximum connections (" + info.maxConnections + ") reached");
    },
    scope: SynchEduca.ViewPhaserWidget.SCOPE_ARRIVE
};

SynchEduca.ViewPhaserWidget.targetAwaitAdvanceEndpointOptions = {
    isSource: false,
    isTarget: true,
    endpoint: ["Dot", {radius: 10}],
    paintStyle: {
        stroke: "#ffe900",
        fill: "transparent",
        radius: 7,
        strokeWidth: 2
    },
    connector: ["Bezier", {curveness: 175}],
    connectorStyle: {strokeWidth: 3, stroke: '#ffe900'},
    maxConnections: SynchEduca.ViewPhaserWidget.MAX_AWAIT_ADVANCE_CONNECTIONS,
    onMaxConnections: function (info, e) {
        alert("Maximum connections (" + info.maxConnections + ") reached");
    },
    scope: SynchEduca.ViewPhaserWidget.SCOPE_AWAIT_ADVANCE
};

SynchEduca.ViewPhaserWidget.targetArriveAwaitAdvanceEndpointOptions = {
    isSource: false,
    isTarget: true,
    endpoint: ["Dot", {radius: 10}],
    paintStyle: {
        stroke: "#ffe900",
        fill: "transparent",
        radius: 7,
        strokeWidth: 2
    },
    connector: ["Bezier", {curveness: 175}],
    connectorStyle: {strokeWidth: 3, stroke: '#ffe900'},
    maxConnections: SynchEduca.ViewPhaserWidget.MAX_ARRIVE_AWAIT_ADVANCE_CONNECTIONS,
    onMaxConnections: function (info, e) {
        alert("Maximum connections (" + info.maxConnections + ") reached");
    },
    scope: SynchEduca.ViewPhaserWidget.SCOPE_ARRIVE_AWAIT_ADVANCE
};

//Common widget handling functionalities
SynchEduca.ViewPhaserWidget.prototype.removeConnections = function () {
    SynchEduca.jsPlumbInstance.remove(this.widgetData.name);
};

SynchEduca.ViewPhaserWidget.prototype.deleteWidgetUi = function () {
    this.parentContainer.deleteWidget(this.widgetData.name);
};

SynchEduca.ViewPhaserWidget.prototype.onDeleteClick = function () {
    var resp = confirm(SynchEduca.strings.MSG_WANT_TO_DELETE_WIDGET);
    if (resp === true) {
        this.presenter.onDeleteWidget();
    }
};

SynchEduca.ViewPhaserWidget.prototype.deleteEndpoint = function (endpoint) {
    SynchEduca.jsPlumbInstance.deleteEndpoint(endpoint);
};

SynchEduca.ViewPhaserWidget.prototype.attachDelete = function () {
    var _this = this;
    $("#" + this.widgetData.name + " ." + SynchEduca.strings.CSS_CLASS_WIDGET_DELETE).click(
        function () {
            _this.onDeleteClick();
            return false; //if attached on <a href ...>
        }
    );
};

SynchEduca.ViewPhaserWidget.prototype.showCount = function (widgetId, registeredCount, unarrivedCount) {
    $('#' + widgetId + "_phaser_registered_count").text(registeredCount);
    $('#' + widgetId + "_phaser_unarrived_count").text(unarrivedCount);
};

SynchEduca.ViewPhaserWidget.prototype.addPhaserRegisterEndpoint = function (widgetId) {
    var endpoint = SynchEduca.jsPlumbInstance.addEndpoint(widgetId, {anchor: [0, 180. / 200, -1, 0]}, SynchEduca.ViewPhaserWidget.targetRegisterEndpointOptions);
    return endpoint;
};

SynchEduca.ViewPhaserWidget.prototype.addPhaserArriveDeregisterEndpoint = function (widgetId) {
    var endpoint = SynchEduca.jsPlumbInstance.addEndpoint(widgetId, {anchor: [1, 180. / 200, 1, 0]}, SynchEduca.ViewPhaserWidget.targetArriveDeregisterEndpointOptions);
    return endpoint;
};

SynchEduca.ViewPhaserWidget.prototype.addPhaserArriveEndpoint = function (widgetId) {
    var endpoint = SynchEduca.jsPlumbInstance.addEndpoint(widgetId, {anchor: [1, 160. / 200, 1, 0]}, SynchEduca.ViewPhaserWidget.targetArriveEndpointOptions);
    return endpoint;
};

SynchEduca.ViewPhaserWidget.prototype.addPhaserAwaitAdvanceEndpoint = function (widgetId) {
    var endpoint = SynchEduca.jsPlumbInstance.addEndpoint(widgetId, {anchor: [1, 140. / 200, 1, 0]}, SynchEduca.ViewPhaserWidget.targetAwaitAdvanceEndpointOptions);
    return endpoint;
};

SynchEduca.ViewPhaserWidget.prototype.addPhaserArriveAwaitAdvanceEndpoint = function (widgetId) {
    var endpoint = SynchEduca.jsPlumbInstance.addEndpoint(widgetId, {anchor: [1, 120. / 200, 1, 0]}, SynchEduca.ViewPhaserWidget.targetArriveAwaitAdvanceEndpointOptions);
    return endpoint;
};