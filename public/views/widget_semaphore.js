var SynchEduca = SynchEduca || {};

// name maps to id of the div containing the widget UI
SynchEduca.ViewSemaphoreWidget = function (parentContainer, widgetData) {
    this.widgetData = widgetData;
    this.parentContainer = parentContainer;
    this.presenter = new SynchEduca.PresenterSemaphoreWidget(this, widgetData);
};

SynchEduca.ViewSemaphoreWidget.SCOPE_RELEASE = "semaphore:release";
SynchEduca.ViewSemaphoreWidget.SCOPE_ACQUIRE = "semaphore:acquire";
SynchEduca.ViewSemaphoreWidget.MAX_ACQUIRE_CONNECTIONS = 100;
SynchEduca.ViewSemaphoreWidget.MAX_RELEASE_CONNECTIONS = 100;

// Singleton endpoint options - same for all widgets
SynchEduca.ViewSemaphoreWidget.sourceReleaseEndpointOptions = {
    isSource: true,
    isTarget: false,
    endpoint: ["Dot", {radius: 10}],
    paintStyle: {
        fill: "blue",
        radius: 7
    },
    connector: ["Bezier", {curveness: 175}],
    connectorStyle: {strokeWidth: 3, stroke: 'blue'},
    scope: SynchEduca.ViewSemaphoreWidget.SCOPE_RELEASE
};

SynchEduca.ViewSemaphoreWidget.sourceAcquireEndpointOptions = {
    isSource: true,
    isTarget: false,
    endpoint: ["Dot", {radius: 10}],
    paintStyle: {
        fill: "blue",
        radius: 7
    },
    connector: ["Bezier", {curveness: 175}],
    connectorStyle: {strokeWidth: 3, stroke: 'blue'},
    scope: SynchEduca.ViewSemaphoreWidget.SCOPE_ACQUIRE
};

SynchEduca.ViewSemaphoreWidget.targetReleaseEndpointOptions = {
    isSource: false,
    isTarget: true,
    endpoint: ["Dot", {radius: 10}],
    paintStyle: {
        stroke: "blue",
        fill: "transparent",
        radius: 7,
        strokeWidth: 2
    },
    connector: ["Bezier", {curveness: 175}],
    connectorStyle: {strokeWidth: 3, stroke: 'blue'},
    maxConnections: SynchEduca.ViewSemaphoreWidget.MAX_RELEASE_CONNECTIONS,
    onMaxConnections: function (info, e) {
        alert("Maximum connections (" + info.maxConnections + ") reached");
    },
    scope: SynchEduca.ViewSemaphoreWidget.SCOPE_RELEASE
};

SynchEduca.ViewSemaphoreWidget.targetAcquireEndpointOptions = {
    isSource: false,
    isTarget: true,
    endpoint: ["Dot", {radius: 10}],
    paintStyle: {
        stroke: "blue",
        fill: "transparent",
        radius: 7,
        strokeWidth: 2
    },
    connector: ["Bezier", {curveness: 175}],
    connectorStyle: {strokeWidth: 3, stroke: 'blue'},
    maxConnections: SynchEduca.ViewSemaphoreWidget.MAX_ACQUIRE_CONNECTIONS,
    onMaxConnections: function (info, e) {
        alert("Maximum connections (" + info.maxConnections + ") reached");
    },
    scope: SynchEduca.ViewSemaphoreWidget.SCOPE_ACQUIRE
};

//Common widget handling functionalities
SynchEduca.ViewSemaphoreWidget.prototype.removeConnections = function () {
    SynchEduca.jsPlumbInstance.remove(this.widgetData.name);
};

SynchEduca.ViewSemaphoreWidget.prototype.deleteWidgetUi = function () {
    this.parentContainer.deleteWidget(this.widgetData.name);
};

SynchEduca.ViewSemaphoreWidget.prototype.onDeleteClick = function () {
    var resp = confirm(SynchEduca.strings.MSG_WANT_TO_DELETE_WIDGET);
    if (resp === true) {
        this.presenter.onDeleteWidget();
    }
};

SynchEduca.ViewSemaphoreWidget.prototype.deleteEndpoint = function (endpoint) {
    SynchEduca.jsPlumbInstance.deleteEndpoint(endpoint);
};

SynchEduca.ViewSemaphoreWidget.prototype.attachDelete = function () {
    var _this = this;
    $("#" + this.widgetData.name + " ." + SynchEduca.strings.CSS_CLASS_WIDGET_DELETE).click(
        function () {
            _this.onDeleteClick();
            return false; //if attached on <a href ...>
        }
    );
};

SynchEduca.ViewSemaphoreWidget.prototype.showCount = function (widgetId, count) {
    $('#' + widgetId + "_semaphore_count").text(count);
};

SynchEduca.ViewSemaphoreWidget.prototype.addSemaphoreReleaseEndpoint = function (widgetId) {
    return SynchEduca.jsPlumbInstance.addEndpoint(widgetId, {anchor: [0, 180. / 200, -1, 0]}, SynchEduca.ViewSemaphoreWidget.targetReleaseEndpointOptions);
};

SynchEduca.ViewSemaphoreWidget.prototype.addSemaphoreAcquireEndpoint = function (widgetId) {
    return SynchEduca.jsPlumbInstance.addEndpoint(widgetId, {anchor: [1, 180. / 200, 1, 0]}, SynchEduca.ViewSemaphoreWidget.targetAcquireEndpointOptions);
};
