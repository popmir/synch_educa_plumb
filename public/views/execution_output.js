var SynchEduca = SynchEduca || {};

SynchEduca.ViewExecutionOutput = function () {
    //this.presenter = SynchEduca.PresenterExecutionOutput();
};
SynchEduca.ViewExecutionOutput.prototype.clearOutput = function (statementOutput) {
    document.getElementById('execution_output').innerHTML = "";
};

SynchEduca.ViewExecutionOutput.prototype.showOutput = function (statementOutput) {
    var newNode = document.createElement('span');
    newNode.innerHTML = statementOutput;
    newNode.className = "statement_output";
    document.getElementById('execution_output').appendChild(newNode);
};