var SynchEduca = SynchEduca || {};

SynchEduca.ViewCreateComponent = function (viewDashboard) {
    this.viewDashboard = viewDashboard;
    this.widgetName = "WorkerName";
    this.latchInitialCount = "2";
    this.barrierInitialCount = "2";
    this.semaphoreInitialCount = "0";
    this.phaserInitialCount = "0";
    this.presenterCreateComponent = new SynchEduca.PresenterCreateComponent(this, viewDashboard);
    this._attachListeners();
};

SynchEduca.ViewCreateComponent.prototype._attachListeners = function () {
    var _this = this;
    //This is best practice, even if copy paste is used
    $("#input_widget_name").on('input', function (e) {
        if (this.value !== '') {
            _this.onWidgetNameChange(this.value);
        }
    });
    $("#input_latch_count").on('input', function (e) {
        _this.onLatchCountChange(this.value);
    });
    $("#input_barrier_count").on('input', function (e) {
        _this.onBarrierCountChange(this.value);
    });
    $("#input_semaphore_count").on('input', function (e) {
        _this.onSemaphoreCountChange(this.value);
    });
    $("#input_phaser_count").on('input', function (e) {
        _this.onPhaserCountChange(this.value);
    });
    $('#select_mechanism input').on('change', function () {
        _this.showMechanismSelected($('#select_mechanism input[name="select_mechanism"]:checked'), $('#select_mechanism input'));
    });
};


//Modify UI looks
SynchEduca.ViewCreateComponent.prototype.showInitialMechanismSelection = function (radioButtonSelected, allRadioButtonsInTheGroup) {
    this.showMechanismSelected($('#select_mechanism input[name="select_mechanism"]:checked'), $('#select_mechanism input'));
};

// radioButtonSelected is a jQuery wrapped element that is selected
// allRadioButtonsInTheGroup is a dictionary object returned by jQuery selector when query matches more elements
SynchEduca.ViewCreateComponent.prototype.showMechanismSelected = function (radioButtonSelected, allRadioButtonsInTheGroup) {
    allRadioButtonsInTheGroup.each(function () {
        var idOfDomElementToMakeInvisible = $(this).val();
        $('#' + idOfDomElementToMakeInvisible).hide();
    });
    var idOfDomElementToMakeVisible = radioButtonSelected.val();
    $('#' + idOfDomElementToMakeVisible).fadeIn();
};

SynchEduca.ViewCreateComponent.prototype.onWidgetNameChange = function (theValue) {
    $("#preview_widget_name").text(theValue);
    this.widgetName = theValue;
};

SynchEduca.ViewCreateComponent.prototype.onLatchCountChange = function (theValue) {
    $("#preview_latch_count").text(theValue);
    this.latchInitialCount = theValue;
};

SynchEduca.ViewCreateComponent.prototype.onBarrierCountChange = function (theValue) {
    $("#preview_barrier_count").text(theValue);
    this.barrierInitialCount = theValue;
};

SynchEduca.ViewCreateComponent.prototype.onSemaphoreCountChange = function (theValue) {
    $("#preview_semaphore_count").text(theValue);
    this.semaphoreInitialCount = theValue;
};

SynchEduca.ViewCreateComponent.prototype.onPhaserCountChange = function (theValue) {
    $("#preview_phaser_count").text(theValue);
    this.phaserInitialCount = theValue;
};

SynchEduca.ViewCreateComponent.prototype.addStatement = function (statementCaption) {
    var li = document.createElement("li");
    li.appendChild(document.createTextNode(statementCaption));
    document.getElementById("preview_statements_list").appendChild(li);
};

SynchEduca.ViewCreateComponent.prototype.removeLastStatement = function () {
    $('#preview_statements_list :last-child').remove();
};

SynchEduca.ViewCreateComponent.prototype.clearAllStatements = function () {
    $('#preview_statements_list').text('');
};

//Processing DOM clicks
SynchEduca.ViewCreateComponent.prototype.onAddWidgetClick = function () {
    this.widgetName = encodeURI(this.widgetName);
    if (this.widgetName.indexOf("#") >= 0 || this.widgetName.indexOf("@") >= 0 || this.widgetName.indexOf("~") >= 0) {
        alert(SynchEduca.strings.MSG_CHARS_NOT_ALLOWED);
        return;
    }
    var validateId = document.getElementById(this.widgetName);
    if (validateId === null) {
        this.presenterCreateComponent.onAddWidget(this.widgetName);
    } else {
        alert(SynchEduca.strings.MSG_IDENTIFIER_IN_USE);
    }
};

SynchEduca.ViewCreateComponent.prototype.onAddLatchWidgetClick = function () {
    if (this.checkInputIsANumber(this.latchInitialCount)) {
        this.presenterCreateComponent.onAddLatchWidget(this.latchInitialCount);
    }
};

SynchEduca.ViewCreateComponent.prototype.onAddBarrierWidgetClick = function () {
    if (this.checkInputIsANumber(this.barrierInitialCount)) {
        this.presenterCreateComponent.onAddBarrierWidget(this.barrierInitialCount);
    }
};

SynchEduca.ViewCreateComponent.prototype.onAddSemaphoreWidgetClick = function () {
    if (this.checkInputIsANumber(this.semaphoreInitialCount)) {
        this.presenterCreateComponent.onAddSemaphoreWidget(this.semaphoreInitialCount);
    }
};

SynchEduca.ViewCreateComponent.prototype.onAddPhaserWidgetClick = function () {
    if (this.checkInputIsANumber(this.phaserInitialCount)) {
        this.presenterCreateComponent.onAddPhaserWidget(this.phaserInitialCount);
    }
};

SynchEduca.ViewCreateComponent.prototype.checkInputIsANumber = function (numberInput) {
    if (numberInput === '') {
        alert("Empty value is not allowed");
        return false;
    }
    if (numberInput.length > 0) {
        for (var i = 0; i < numberInput.length; i++) {
            //Check if any char is not a digit
            if (numberInput.charCodeAt(i) < 48 || numberInput.charCodeAt(i) > 57) {
                alert(SynchEduca.strings.MSG_ONLY_DIGIT);
                return false;
            }
        }
    }
    return true;
};

SynchEduca.ViewCreateComponent.prototype.onAddPlainStatementClick = function () {
    this.presenterCreateComponent.onAddPlainStatement();
};

SynchEduca.ViewCreateComponent.prototype.onAddLatchCountDownStatementClick = function () {
    this.presenterCreateComponent.onAddLatchCountDownStatement();
};

SynchEduca.ViewCreateComponent.prototype.onAddLatchAwaitStatementClick = function () {
    this.presenterCreateComponent.onAddLatchAwaitStatement();
};

SynchEduca.ViewCreateComponent.prototype.onAddBarrierAwaitStatementClick = function () {
    this.presenterCreateComponent.onAddBarrierAwaitStatement();
};

SynchEduca.ViewCreateComponent.prototype.onAddBarrierOnZeroStatementClick = function () {
    this.presenterCreateComponent.onAddBarrierOnZeroStatement();
};

SynchEduca.ViewCreateComponent.prototype.onAddSemaphoreReleaseStatementClick = function () {
    var permits = prompt("How many permits?", "1");
    if (permits !== null && this.checkInputIsANumber(permits)) {
        this.presenterCreateComponent.onAddSemaphoreReleaseStatement(permits);
    }
};

SynchEduca.ViewCreateComponent.prototype.onAddSemaphoreAcquireStatementClick = function () {
    var permits = prompt("How many permits?", "1");
    if (permits !== null && this.checkInputIsANumber(permits)) {
        this.presenterCreateComponent.onAddSemaphoreAcquireStatement(permits);
    }
};

SynchEduca.ViewCreateComponent.prototype.onAddPhaserRegisterStatementClick = function () {
    this.presenterCreateComponent.onAddPhaserRegisterStatement();
};

SynchEduca.ViewCreateComponent.prototype.onAddPhaserArriveDeregisterStatementClick = function () {
    this.presenterCreateComponent.onAddPhaserArriveDeregisterStatement();
};

SynchEduca.ViewCreateComponent.prototype.onAddPhaserArriveStatementClick = function () {
    this.presenterCreateComponent.onAddPhaserArriveStatement();
};

SynchEduca.ViewCreateComponent.prototype.onAddPhaserAwaitAdvanceStatementClick = function () {
    this.presenterCreateComponent.onAddPhaserAwaitAdvanceStatement();
};

SynchEduca.ViewCreateComponent.prototype.onAddPhaserArriveAwaitAdvanceStatementClick = function () {
    this.presenterCreateComponent.onAddPhaserArriveAwaitAdvanceStatement();
};

SynchEduca.ViewCreateComponent.prototype.onRemoveLastStatementClick = function () {
    this.presenterCreateComponent.onRemoveLastStatement();
};

SynchEduca.ViewCreateComponent.prototype.onClearAllStatementsClick = function () {
    this.presenterCreateComponent.onClearAllStatements();
};

//Wiring DOM clicks to SynchEduca.ViewCreateComponent functions
$('#add_widget').click(function () {
    SynchEduca.viewCreateComponent.onAddWidgetClick();
    return false;
});

$('#add_latch').click(function () {
    SynchEduca.viewCreateComponent.onAddLatchWidgetClick();
    return false;
});

$('#add_barrier').click(function () {
    SynchEduca.viewCreateComponent.onAddBarrierWidgetClick();
    return false;
});

$('#add_semaphore').click(function () {
    SynchEduca.viewCreateComponent.onAddSemaphoreWidgetClick();
    return false;
});

$('#add_phaser').click(function () {
    SynchEduca.viewCreateComponent.onAddPhaserWidgetClick();
    return false;
});

$('#add_plain_statement').click(function () {
    SynchEduca.viewCreateComponent.onAddPlainStatementClick();
    return false;
});

$('#add_latch_countdown_statement').click(function () {
    SynchEduca.viewCreateComponent.onAddLatchCountDownStatementClick();
    return false;
});

$('#add_latch_await_statement').click(function () {
    SynchEduca.viewCreateComponent.onAddLatchAwaitStatementClick();
    return false;
});

$('#add_barrier_await_statement').click(function () {
    SynchEduca.viewCreateComponent.onAddBarrierAwaitStatementClick();
    return false;
});

$('#add_barrier_onzero_statement').click(function () {
    SynchEduca.viewCreateComponent.onAddBarrierOnZeroStatementClick();
    return false;
});

$('#add_semaphore_release_statement').click(function () {
    SynchEduca.viewCreateComponent.onAddSemaphoreReleaseStatementClick();
    return false;
});

$('#add_semaphore_acquire_statement').click(function () {
    SynchEduca.viewCreateComponent.onAddSemaphoreAcquireStatementClick();
    return false;
});

$('#add_phaser_register_statement').click(function () {
    SynchEduca.viewCreateComponent.onAddPhaserRegisterStatementClick();
    return false;
});

$('#add_phaser_arrive_deregister_statement').click(function () {
    SynchEduca.viewCreateComponent.onAddPhaserArriveDeregisterStatementClick();
    return false;
});

$('#add_phaser_arrive_statement').click(function () {
    SynchEduca.viewCreateComponent.onAddPhaserArriveStatementClick();
    return false;
});

$('#add_phaser_await_advance_statement').click(function () {
    SynchEduca.viewCreateComponent.onAddPhaserAwaitAdvanceStatementClick();
    return false;
});

$('#add_phaser_arrive_await_advance_statement').click(function () {
    SynchEduca.viewCreateComponent.onAddPhaserArriveAwaitAdvanceStatementClick();
    return false;
});

$('#preview_remove_statement').click(function () {
    SynchEduca.viewCreateComponent.onRemoveLastStatementClick();
    return false;
});

$('#preview_clear_all').click(function () {
    SynchEduca.viewCreateComponent.onClearAllStatementsClick();
    return false;
});