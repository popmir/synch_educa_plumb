var SynchEduca = SynchEduca || {};

// name maps to id of the div containing the widget UI
SynchEduca.ViewBarrierWidget = function (parentContainer, widgetData) {
    this.widgetData = widgetData;
    this.parentContainer = parentContainer;
    this.presenter = new SynchEduca.PresenterBarrierWidget(this, widgetData);
};

SynchEduca.ViewBarrierWidget.SCOPE_AWAIT = "barrier:await";
SynchEduca.ViewBarrierWidget.SCOPE_ON_ZERO = "barrier:on_zero";
SynchEduca.ViewBarrierWidget.MAX_ON_ZERO_CONNECTIONS = 100;
SynchEduca.ViewBarrierWidget.MAX_AWAIT_CONNECTIONS = 100;

// Singleton endpoint options - same for all widgets
SynchEduca.ViewBarrierWidget.sourceAwaitEndpointOptions = {
    isSource: true,
    isTarget: false,
    endpoint: ["Dot", {radius: 10}],
    paintStyle: {
        fill: "brown",
        radius: 7
    },
    connector: ["Bezier", {curveness: 175}],
    connectorStyle: {strokeWidth: 3, stroke: 'brown'},
    scope: SynchEduca.ViewBarrierWidget.SCOPE_AWAIT
};

SynchEduca.ViewBarrierWidget.sourceOnZeroEndpointOptions = {
    isSource: true,
    isTarget: false,
    endpoint: ["Dot", {radius: 10}],
    paintStyle: {
        fill: "brown",
        radius: 7
    },
    connector: ["Bezier", {curveness: 175}],
    connectorStyle: {strokeWidth: 3, stroke: 'brown'},
    scope: SynchEduca.ViewBarrierWidget.SCOPE_ON_ZERO
};

SynchEduca.ViewBarrierWidget.targetAwaitEndpointOptions = {
    isSource: false,
    isTarget: true,
    endpoint: ["Dot", {radius: 10}],
    paintStyle: {
        stroke: "brown",
        fill: "transparent",
        radius: 7,
        strokeWidth: 2
    },
    connector: ["Bezier", {curveness: 175}],
    connectorStyle: {strokeWidth: 3, stroke: 'brown'},
    maxConnections: SynchEduca.ViewBarrierWidget.MAX_AWAIT_CONNECTIONS,
    onMaxConnections: function (info, e) {
        alert("Maximum connections (" + info.maxConnections + ") reached");
    },
    scope: SynchEduca.ViewBarrierWidget.SCOPE_AWAIT
};

SynchEduca.ViewBarrierWidget.targetOnZeroEndpointOptions = {
    isSource: false,
    isTarget: true,
    endpoint: ["Dot", {radius: 10}],
    paintStyle: {
        stroke: "brown",
        fill: "transparent",
        radius: 7,
        strokeWidth: 2
    },
    connector: ["Bezier", {curveness: 175}],
    connectorStyle: {strokeWidth: 3, stroke: 'brown'},
    maxConnections: SynchEduca.ViewBarrierWidget.MAX_ON_ZERO_CONNECTIONS,
    onMaxConnections: function (info, e) {
        alert("Maximum connections (" + info.maxConnections + ") reached");
    },
    scope: SynchEduca.ViewBarrierWidget.SCOPE_ON_ZERO
};

//Common widget handling functionalities
SynchEduca.ViewBarrierWidget.prototype.removeConnections = function () {
    SynchEduca.jsPlumbInstance.remove(this.widgetData.name);
};

SynchEduca.ViewBarrierWidget.prototype.deleteWidgetUi = function () {
    this.parentContainer.deleteWidget(this.widgetData.name);
};

SynchEduca.ViewBarrierWidget.prototype.onDeleteClick = function () {
    var resp = confirm(SynchEduca.strings.MSG_WANT_TO_DELETE_WIDGET);
    if (resp === true) {
        this.presenter.onDeleteWidget();
    }
};

SynchEduca.ViewBarrierWidget.prototype.deleteEndpoint = function (endpoint) {
    SynchEduca.jsPlumbInstance.deleteEndpoint(endpoint);
};

SynchEduca.ViewBarrierWidget.prototype.attachDelete = function () {
    var _this = this;
    $("#" + this.widgetData.name + " ." + SynchEduca.strings.CSS_CLASS_WIDGET_DELETE).click(
        function () {
            _this.onDeleteClick();
            return false; //if attached on <a href ...>
        }
    );
};

SynchEduca.ViewBarrierWidget.prototype.showCount = function (widgetId, count) {
    $('#' + widgetId + "_barrier_count").text(count);
};

SynchEduca.ViewBarrierWidget.prototype.addBarrierAwaitEndpoint = function (widgetId) {
    var endpoint = SynchEduca.jsPlumbInstance.addEndpoint(widgetId, {anchor: [0, 180. / 200, -1, 0]}, SynchEduca.ViewBarrierWidget.targetAwaitEndpointOptions);
    return endpoint;
};

SynchEduca.ViewBarrierWidget.prototype.addBarrierOnZeroEndpoint = function (widgetId) {
    var endpoint = SynchEduca.jsPlumbInstance.addEndpoint(widgetId, {anchor: [1, 180. / 200, 1, 0]}, SynchEduca.ViewBarrierWidget.targetOnZeroEndpointOptions);
    return endpoint;
};
