var SynchEduca = SynchEduca || {};


SynchEduca.ViewStopwatch = function () {
  this.intervalToTimeout = {};
  this.buttonStart = document.getElementById('button-start');
  this.buttonStop = document.getElementById('button-stop');
  this.buttonReset = document.getElementById('button-reset');
  this.presenter = new SynchEduca.PresenterStopwatch(this);
  this.initUi();
};

SynchEduca.ViewStopwatch.seconds = 0;
SynchEduca.ViewStopwatch.minutes = 0;
SynchEduca.ViewStopwatch.hours = 0;  

SynchEduca.ViewStopwatch.appendHours = document.getElementById("hours");
SynchEduca.ViewStopwatch.appendMinutes = document.getElementById("minutes");
SynchEduca.ViewStopwatch.appendSeconds = document.getElementById("seconds");

SynchEduca.ViewStopwatch.prototype.initUi = function () {
  var _this = this;
  this.buttonStart.onclick = function() {    
     clearInterval(_this.intervalToTimeout);
     _this.intervalToTimeout = setInterval(_this.startTimer, 1000);
  }
  
  this.buttonStop.onclick = function() {
       clearInterval(_this.intervalToTimeout);
  }

  this.buttonReset.onclick = function() {
    clearInterval(_this.intervalToTimeout);
    SynchEduca.ViewStopwatch.hours = 0;
    SynchEduca.ViewStopwatch.minutes = 0;
    SynchEduca.ViewStopwatch.seconds = 0;
    SynchEduca.ViewStopwatch.appendHours.innerHTML = "00";
    SynchEduca.ViewStopwatch.appendMinutes.innerHTML = "00";
    SynchEduca.ViewStopwatch.appendSeconds.innerHTML = "00";
  }
  
   
}
  
SynchEduca.ViewStopwatch.prototype.startTimer = function() {
  SynchEduca.ViewStopwatch.seconds++;   
  if(SynchEduca.ViewStopwatch.seconds <= 9){
    SynchEduca.ViewStopwatch.appendSeconds.innerHTML = "0" + SynchEduca.ViewStopwatch.seconds;
  } else {
    SynchEduca.ViewStopwatch.appendSeconds.innerHTML = SynchEduca.ViewStopwatch.seconds;
  } 
  
  if (SynchEduca.ViewStopwatch.seconds > 59) {    
    SynchEduca.ViewStopwatch.minutes++;    
    SynchEduca.ViewStopwatch.seconds = 0;
    SynchEduca.ViewStopwatch.appendSeconds.innerHTML = "00";
  }
  
  if (SynchEduca.ViewStopwatch.minutes <= 9) {
    SynchEduca.ViewStopwatch.appendMinutes.innerHTML = "0" + SynchEduca.ViewStopwatch.minutes;
  } else {
    SynchEduca.ViewStopwatch.appendMinutes.innerHTML = SynchEduca.ViewStopwatch.minutes;
  }

  if (SynchEduca.ViewStopwatch.minutes > 59) {
    SynchEduca.ViewStopwatch.hours++;
    SynchEduca.ViewStopwatch.minutes = 0;
    SynchEduca.ViewStopwatch.appendMinutes.innerHTML = "00";
  }

}

// window.onload = function () {  
//   var seconds = 00; 
//   var tens = 00; 
//   var Interval ;

//   var appendTens = document.getElementById("tens")
//   var appendSeconds = document.getElementById("seconds")
//   var buttonStart = document.getElementById('button-start');
//   var buttonStop = document.getElementById('button-stop');
//   var buttonReset = document.getElementById('button-reset');
// }