var SynchEduca = SynchEduca || {};

/*
name maps to id of the div containing the widget UI
*/
SynchEduca.ViewWidgetNormal = function (parentContainer, widgetData) {
    this.widgetData = widgetData;
    this.parentContainer = parentContainer;
    this.presenter = new SynchEduca.PresenterWidgetNormal(this, widgetData);
};

//TODO make this correspond to css class height, make setting it from code
// HEIGHT must be consistent with CSS style .group-container 
SynchEduca.ViewWidgetNormal.HEIGHT = 210.;
// STATEMENT_HEIGHT must be consistenet with CSS style .group-container ul li 
SynchEduca.ViewWidgetNormal.STATEMENT_HEIGHT = 21.;
SynchEduca.ViewWidgetNormal.HALF_STATEMENT_HEIGHT = 0.5;
SynchEduca.ViewWidgetNormal.TOPBAR_HEIGHT = 30;

SynchEduca.ViewWidgetNormal.prototype.removeConnections = function () {
    SynchEduca.jsPlumbInstance.remove(this.widgetData.name);
};

SynchEduca.ViewWidgetNormal.prototype.deleteWidgetUi = function () {
    this.parentContainer.deleteWidget(this.widgetData.name);
};

SynchEduca.ViewWidgetNormal.prototype.onDeleteClick = function () {
    var resp = confirm(SynchEduca.strings.MSG_WANT_TO_DELETE_WIDGET);
    if (resp === true) {
        //This removes all endpoints and its connections of the widget
        this.presenter.onDeleteWidget();
    }
};

SynchEduca.ViewWidgetNormal.prototype.deleteEndpoint = function (endpoint) {
    SynchEduca.jsPlumbInstance.deleteEndpoint(endpoint);
};

SynchEduca.ViewWidgetNormal.prototype.attachDelete = function () {
    var _this = this;
    $("#" + this.widgetData.name + " ." + SynchEduca.strings.CSS_CLASS_WIDGET_DELETE).click(
        function () {
            _this.onDeleteClick();
            return false; //if attached on <a href ...>
        }
    );
};

SynchEduca.ViewWidgetNormal.prototype.addTopEndpoint = function (widgetId) {
    this.parentContainer.addTopEndpoint(widgetId, {anchor: "TopCenter"});
};

SynchEduca.ViewWidgetNormal.prototype.addBottomEndpoint = function (widgetId) {
    this.parentContainer.addBottomEndpoint(widgetId, {anchor: "BottomCenter"});
};

SynchEduca.ViewWidgetNormal.prototype.addStatementLatchCountDownEndpoint = function (widgetId, statementIndex) {
    var anchorData = {anchor: SynchEduca.ViewWidgetNormal.Utils.getWidgetRightAnchor(statementIndex)};
    this.parentContainer.addStatementEndpoint(widgetId, anchorData, statementIndex, SynchEduca.ViewLatchWidget.sourceCountDownEndpointOptions);
};

SynchEduca.ViewWidgetNormal.prototype.addStatementLatchAwaitEndpoint = function (widgetId, statementIndex) {
    var anchorData = {anchor: SynchEduca.ViewWidgetNormal.Utils.getWidgetLeftAnchor(statementIndex)};
    this.parentContainer.addStatementEndpoint(widgetId, anchorData, statementIndex, SynchEduca.ViewLatchWidget.sourceAwaitEndpointOptions);
};

SynchEduca.ViewWidgetNormal.prototype.addStatementBarrierAwaitEndpoint = function (widgetId, statementIndex) {
    var anchorData = {anchor: SynchEduca.ViewWidgetNormal.Utils.getWidgetRightAnchor(statementIndex)};
    this.parentContainer.addStatementEndpoint(widgetId, anchorData, statementIndex, SynchEduca.ViewBarrierWidget.sourceAwaitEndpointOptions);
};

SynchEduca.ViewWidgetNormal.prototype.addStatementBarrierOnZeroEndpoint = function (widgetId, statementIndex) {
    var anchorData = {anchor: SynchEduca.ViewWidgetNormal.Utils.getWidgetLeftAnchor(statementIndex)};
    this.parentContainer.addStatementEndpoint(widgetId, anchorData, statementIndex, SynchEduca.ViewBarrierWidget.sourceOnZeroEndpointOptions);
};

SynchEduca.ViewWidgetNormal.prototype.addStatementSemaphoreReleaseEndpoint = function (widgetId, statementIndex) {
    var anchorData = {anchor: SynchEduca.ViewWidgetNormal.Utils.getWidgetRightAnchor(statementIndex)};
    this.parentContainer.addStatementEndpoint(widgetId, anchorData, statementIndex, SynchEduca.ViewSemaphoreWidget.sourceReleaseEndpointOptions);
};

SynchEduca.ViewWidgetNormal.prototype.addStatementSemaphoreAcquireEndpoint = function (widgetId, statementIndex) {
    var anchorData = {anchor: SynchEduca.ViewWidgetNormal.Utils.getWidgetLeftAnchor(statementIndex)};
    this.parentContainer.addStatementEndpoint(widgetId, anchorData, statementIndex, SynchEduca.ViewSemaphoreWidget.sourceAcquireEndpointOptions);
};

SynchEduca.ViewWidgetNormal.prototype.addStatementPhaserRegisterEndpoint = function (widgetId, statementIndex) {
    var anchorData = {anchor: SynchEduca.ViewWidgetNormal.Utils.getWidgetRightAnchor(statementIndex)};
    this.parentContainer.addStatementEndpoint(widgetId, anchorData, statementIndex, SynchEduca.ViewPhaserWidget.sourceRegisterEndpointOptions);
};

SynchEduca.ViewWidgetNormal.prototype.addStatementPhaserArriveDeregisterEndpoint = function (widgetId, statementIndex) {
    var anchorData = {anchor: SynchEduca.ViewWidgetNormal.Utils.getWidgetLeftAnchor(statementIndex)};
    this.parentContainer.addStatementEndpoint(widgetId, anchorData, statementIndex, SynchEduca.ViewPhaserWidget.sourceArriveDeregisterEndpointOptions);
};

SynchEduca.ViewWidgetNormal.prototype.addStatementPhaserArriveEndpoint = function (widgetId, statementIndex) {
    var anchorData = {anchor: SynchEduca.ViewWidgetNormal.Utils.getWidgetLeftAnchor(statementIndex)};
    this.parentContainer.addStatementEndpoint(widgetId, anchorData, statementIndex, SynchEduca.ViewPhaserWidget.sourceArriveEndpointOptions);
};

SynchEduca.ViewWidgetNormal.prototype.addStatementPhaserAwaitAdvanceEndpoint = function (widgetId, statementIndex) {
    var anchorData = {anchor: SynchEduca.ViewWidgetNormal.Utils.getWidgetLeftAnchor(statementIndex)};
    this.parentContainer.addStatementEndpoint(widgetId, anchorData, statementIndex, SynchEduca.ViewPhaserWidget.sourceAwaitAdvanceEndpointOptions);
};

SynchEduca.ViewWidgetNormal.prototype.addStatementPhaserArriveAwaitAdvanceEndpoint = function (widgetId, statementIndex) {
    var anchorData = {anchor: SynchEduca.ViewWidgetNormal.Utils.getWidgetLeftAnchor(statementIndex)};
    this.parentContainer.addStatementEndpoint(widgetId, anchorData, statementIndex, SynchEduca.ViewPhaserWidget.sourceArriveAwaitAdvanceEndpointOptions);
};

SynchEduca.ViewWidgetNormal.prototype.showOutput = function (statementOutput) {
    this.parentContainer.showOutput(statementOutput);
};

SynchEduca.ViewWidgetNormal.Utils = {    
    getWidgetRightAnchor: function (statementIndex) {
        return [1, SynchEduca.ViewWidgetNormal.TOPBAR_HEIGHT / SynchEduca.ViewWidgetNormal.HEIGHT
            + (statementIndex * SynchEduca.ViewWidgetNormal.STATEMENT_HEIGHT
            + SynchEduca.ViewWidgetNormal.STATEMENT_HEIGHT * SynchEduca.ViewWidgetNormal.HALF_STATEMENT_HEIGHT)
            / SynchEduca.ViewWidgetNormal.HEIGHT,
            1, 0];
    },
    getWidgetLeftAnchor: function (statementIndex) {
        return [0, SynchEduca.ViewWidgetNormal.TOPBAR_HEIGHT / SynchEduca.ViewWidgetNormal.HEIGHT
            + (statementIndex * SynchEduca.ViewWidgetNormal.STATEMENT_HEIGHT
            + SynchEduca.ViewWidgetNormal.STATEMENT_HEIGHT * SynchEduca.ViewWidgetNormal.HALF_STATEMENT_HEIGHT) 
            / SynchEduca.ViewWidgetNormal.HEIGHT,
            -1, 0];
    }
};