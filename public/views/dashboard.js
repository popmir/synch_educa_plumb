var SynchEduca = SynchEduca || {};

SynchEduca.ViewDashboard = function (executionOutput) {
    this.executionOutput = executionOutput;
    this.normalWidgetViews = {}; // dictionary containing all ViewWidgetNormal objects
    this.presenter = new SynchEduca.PresenterDashboard(this);

    this.sourceEndpointOptions = {
        isSource: true,
        isTarget: false,
        endpoint: ["Dot", {radius: 10}],
        paintStyle: {
            fill: "#7AB02C",
            radius: 7
        },
        connector: ["Bezier", {curveness: 175}],
        connectorStyle: {strokeWidth: 3, stroke: 'green'},
        scope: "bottom2Top"
    };

    this.targetEndpointOptions = {
        isSource: false,
        isTarget: true,
        endpoint: ["Dot", {radius: 10}],
        paintStyle: {
            stroke: "#7AB02C",
            fill: "transparent",
            radius: 7,
            strokeWidth: 2
        },
        connector: ["Bezier", {curveness: 175}],
        connectorStyle: {strokeWidth: 3, stroke: 'green'},
        scope: "bottom2Top"
    };
};

SynchEduca.ViewDashboard.DELAY_ADDING_ENDPOINT = 10 //millisec



SynchEduca.ViewDashboard.prototype.showWidget = function (widgetHtml, viewWidget) {
    $("#canvas").append(widgetHtml);
};

SynchEduca.ViewDashboard.prototype.makeWidgetDraggable = function (widgetId) {
    SynchEduca.jsPlumbInstance.draggable(widgetId, {containment: true});
};

//attachment responsibility passed to its class
SynchEduca.ViewDashboard.prototype.attachWidgetView = function (widgetData) {
    this.normalWidgetViews[widgetData.name] = new SynchEduca.ViewWidgetNormal(this, widgetData);
};

SynchEduca.ViewDashboard.prototype.attachLatchView = function (widgetData) {
    this.normalWidgetViews[widgetData.name] = new SynchEduca.ViewLatchWidget(this, widgetData);
};

SynchEduca.ViewDashboard.prototype.attachBarrierView = function (widgetData) {
    this.normalWidgetViews[widgetData.name] = new SynchEduca.ViewBarrierWidget(this, widgetData);
};

SynchEduca.ViewDashboard.prototype.attachSemaphoreView = function (widgetData) {
    this.normalWidgetViews[widgetData.name] = new SynchEduca.ViewSemaphoreWidget(this, widgetData);
};

SynchEduca.ViewDashboard.prototype.attachPhaserView = function (widgetData) {
    this.normalWidgetViews[widgetData.name] = new SynchEduca.ViewPhaserWidget(this, widgetData);
};

SynchEduca.ViewDashboard.prototype.deleteWidget = function (widgetId) {
    $("#" + widgetId).remove();
};

SynchEduca.ViewDashboard.prototype.addTopEndpoint = function (widgetId, anchor) {
    var jsPlumbEndpoint = SynchEduca.jsPlumbInstance.addEndpoint(widgetId, anchor, this.targetEndpointOptions);
        //Solution that checks if the same id is generated and then retries after few millis
    if (this.presenter.checkIfExists(jsPlumbEndpoint.getId())) {
        SynchEduca.jsPlumbInstance.deleteEndpoint(jsPlumbEndpoint);
        setTimeout(() => {
            this.addStatementEndpoint(widgetId, anchor, positionId, endpointOptions)
        }, SynchEduca.ViewDashboard.DELAY_ADDING_ENDPOINT);
    } else {
        return this.presenter.addTopEndpoint(jsPlumbEndpoint.getId(), widgetId, jsPlumbEndpoint);
    }
};

SynchEduca.ViewDashboard.prototype.addBottomEndpoint = function (widgetId, anchor) {
    var jsPlumbEndpoint = SynchEduca.jsPlumbInstance.addEndpoint(widgetId, anchor, this.sourceEndpointOptions);
        //Solution that checks if the same id is generated and then retries after few millis
    if (this.presenter.checkIfExists(jsPlumbEndpoint.getId())) {
        SynchEduca.jsPlumbInstance.deleteEndpoint(jsPlumbEndpoint);
        setTimeout(() => {
            this.addStatementEndpoint(widgetId, anchor, positionId, endpointOptions)
        }, SynchEduca.ViewDashboard.DELAY_ADDING_ENDPOINT);
    } else {
        return this.presenter.addBottomEndpoint(jsPlumbEndpoint.getId(), widgetId, jsPlumbEndpoint);
    }
};

SynchEduca.ViewDashboard.prototype.addStatementEndpoint = function (widgetId, anchor, positionId, endpointOptions) {
    var jsPlumbEndpoint = SynchEduca.jsPlumbInstance.addEndpoint(widgetId, anchor, endpointOptions);
    //.This code is causing problems.
    //
    // return this.presenter.addStatementEndpoint(endpoint.getId(), widgetId, positionId, endpoint);
    //
    // It seams it is not safe to use JS plumb getId. I think it does not garanty uniquness
    // If two endpoints are added in a very short time frame.
    // Problem manifests it self with some endpoints not getting registered in the dashbard model, 
    // so the model counts less endpoints than there should be
    // One way to solve this is to use UUID from 
    // https://stackoverflow.com/questions/105034/create-guid-uuid-in-javascript
    // function generateUUID() { // Public Domain/MIT
    //     var d = new Date().getTime();
    //     if (typeof performance !== 'undefined' && typeof performance.now === 'function'){
    //         d += performance.now(); //use high-precision timer if available
    //     }
    //     return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    //         var r = (d + Math.random() * 16) % 16 | 0;
    //         d = Math.floor(d / 16);
    //         return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    //     });
    // }


    // However, the above UUID needs to be integrated into jsPlumb for us to be useful
    //Solution that checks if the same id is generated and then retries after few millis
    if (this.presenter.checkIfExists(jsPlumbEndpoint.getId())) {
        SynchEduca.jsPlumbInstance.deleteEndpoint(jsPlumbEndpoint);
        setTimeout(() => {
            this.addStatementEndpoint(widgetId, anchor, positionId, endpointOptions)
        }, SynchEduca.ViewDashboard.DELAY_ADDING_ENDPOINT);
    } else {
        return this.presenter.addStatementEndpoint(jsPlumbEndpoint.getId(), widgetId, positionId, jsPlumbEndpoint);
    }
};

//Attach jsPlumb listeners and on click listeners for start
SynchEduca.ViewDashboard.prototype.attachListeners = function () {
    var _this = this;
    this._attachDeleteConnections();
    this._attachOnConnectionEstablish();
    //Wiring DOM click to start execution functions
    $("#start_execution").click(function () {
        _this.executionOutput.clearOutput();
        SynchEduca.scheduler.startExecution();
        return false;
    });
};

SynchEduca.ViewDashboard.prototype.showOutput = function (statementOutput) {
    this.executionOutput.showOutput(statementOutput);
};

SynchEduca.ViewDashboard.prototype.deleteConnection = function (connection) {
    SynchEduca.jsPlumbInstance.deleteConnection(connection);
};
SynchEduca.ViewDashboard.prototype._attachDeleteConnections = function () {
    var _this = this;
    // bind a click listener to each connection; the connection is deleted.
    SynchEduca.jsPlumbInstance.bind("click", function (connection) {
        _this.deleteConnection(connection);
    });
};

SynchEduca.ViewDashboard.prototype.onConnectionEstablished = function (connection) {
    //validate connection
    if (connection.sourceId === connection.targetId) {
        alert(SynchEduca.strings.MSG_CONN_NOT_ALLOWED_ON_SELF);
        this.deleteConnection(connection);
    } else {
        this.presenter.onConnectionEstablished(connection);
    }
};

SynchEduca.ViewDashboard.prototype._attachOnConnectionEstablish = function () {
    var _this = this;
    // bind a connection listener. note that the parameter passed to this function contains more than
    // just the new connection - see the documentation for a full list of what is included in 'info'.
    SynchEduca.jsPlumbInstance.bind("connection", function (info) {
        //info.connection.getOverlay("label").setLabel(info.connection.id);
        _this.onConnectionEstablished(info.connection);
    });
};

