var SynchEduca = SynchEduca || {};

/*
name maps to id of the div containing the widget UI
*/
SynchEduca.ViewLatchWidget = function (parentContainer, widgetData) {
    this.widgetData = widgetData;
    this.parentContainer = parentContainer;
    this.presenter = new SynchEduca.PresenterLatchWidget(this, widgetData);
};

SynchEduca.ViewLatchWidget.SCOPE_AWAIT = "latch:await";
SynchEduca.ViewLatchWidget.SCOPE_COUNT_DOWN = "latch:count_down";
SynchEduca.ViewLatchWidget.MAX_AWAIT_CONNECTIONS = 100;

//Singleton endpoint options - same for all widgets
SynchEduca.ViewLatchWidget.targetCountDownEndpointOptions = {
    isSource: false,
    isTarget: true,
    endpoint: ["Dot", {radius: 10}],
    paintStyle: {
        stroke: "orange",
        fill: "transparent",
        radius: 7,
        strokeWidth: 2
    },
    connector: ["Bezier", {curveness: 175}],
    connectorStyle: {strokeWidth: 3, stroke: 'orange'},
    maxConnections: 2,
    onMaxConnections: function (info, e) {
        alert("Maximum connections (" + info.maxConnections + ") reached");
    },
    scope: SynchEduca.ViewLatchWidget.SCOPE_COUNT_DOWN
};

SynchEduca.ViewLatchWidget.sourceAwaitEndpointOptions = {
    isSource: true,
    isTarget: false,
    endpoint: ["Dot", {radius: 10}],
    paintStyle: {
        fill: "orange",
        radius: 7
    },
    connector: ["Bezier", {curveness: 175}],
    connectorStyle: {strokeWidth: 3, stroke: 'orange'},
    scope: SynchEduca.ViewLatchWidget.SCOPE_AWAIT
};

SynchEduca.ViewLatchWidget.sourceCountDownEndpointOptions = {
    isSource: true,
    isTarget: false,
    endpoint: ["Dot", {radius: 10}],
    paintStyle: {
        fill: "orange",
        radius: 7
    },
    connector: ["Bezier", {curveness: 175}],
    connectorStyle: {strokeWidth: 3, stroke: 'orange'},
    scope: SynchEduca.ViewLatchWidget.SCOPE_COUNT_DOWN
};

SynchEduca.ViewLatchWidget.targetAwaitEndpointOptions = {
    isSource: false,
    isTarget: true,
    endpoint: ["Dot", {radius: 10}],
    paintStyle: {
        stroke: "orange",
        fill: "transparent",
        radius: 7,
        strokeWidth: 2
    },
    connector: ["Bezier", {curveness: 175}],
    connectorStyle: {strokeWidth: 3, stroke: 'orange'},
    maxConnections: SynchEduca.ViewLatchWidget.MAX_AWAIT_CONNECTIONS,
    onMaxConnections: function (info, e) {
        alert("Maximum connections (" + info.maxConnections + ") reached");
    },
    scope: SynchEduca.ViewLatchWidget.SCOPE_AWAIT
};

//Common widget handling functionalities
SynchEduca.ViewLatchWidget.prototype.removeConnections = function () {
    SynchEduca.jsPlumbInstance.remove(this.widgetData.name);
};

SynchEduca.ViewLatchWidget.prototype.deleteWidgetUi = function () {
    this.parentContainer.deleteWidget(this.widgetData.name);
};

SynchEduca.ViewLatchWidget.prototype.onDeleteClick = function () {
    var resp = confirm(SynchEduca.strings.MSG_WANT_TO_DELETE_WIDGET);
    if (resp === true) {
        this.presenter.onDeleteWidget();
    }
};

SynchEduca.ViewLatchWidget.prototype.deleteEndpoint = function (endpoint) {
    SynchEduca.jsPlumbInstance.deleteEndpoint(endpoint);
};

SynchEduca.ViewLatchWidget.prototype.attachDelete = function () {
    var _this = this;
    $("#" + this.widgetData.name + " ." + SynchEduca.strings.CSS_CLASS_WIDGET_DELETE).click(
        function () {
            _this.onDeleteClick();
            return false; //if attached on <a href ...>
        }
    );
};

//Specific Latch functionalities
SynchEduca.ViewLatchWidget.prototype.setMaxCountDownConnections = function (count) {
    SynchEduca.ViewLatchWidget.targetCountDownEndpointOptions.maxConnections = count;
};

SynchEduca.ViewLatchWidget.prototype.showCount = function (widgetId, count) {
    $('#' + widgetId + "_latch_count").text(count);
};

SynchEduca.ViewLatchWidget.prototype.addLatchCountDownEndpoint = function (widgetId) {
    var endpoint = SynchEduca.jsPlumbInstance.addEndpoint(widgetId, {anchor: [0, 180. / 200, -1, 0]}, SynchEduca.ViewLatchWidget.targetCountDownEndpointOptions);
    return endpoint;
};

SynchEduca.ViewLatchWidget.prototype.addLatchAwaitEndpoint = function (widgetId) {
    var endpoint = SynchEduca.jsPlumbInstance.addEndpoint(widgetId, {anchor: [1, 180. / 200, 1, 0]}, SynchEduca.ViewLatchWidget.targetAwaitEndpointOptions);
    return endpoint;
};