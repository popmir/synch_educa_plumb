const express = require('express');
var path = require('path');
const app = express();

app.use('/synch_educa', express.static(path.join(__dirname, 'public')));
app.use('/synch_educa/views', express.static(path.join(path.join(__dirname, 'public'), 'views')));
app.use('/synch_educa/presenters', express.static(path.join(path.join(__dirname, 'public'), 'presenters')));
app.use('/synch_educa/models', express.static(path.join(path.join(__dirname, 'public'), 'models')));


app.get('/', function (req, res) {
    res.send('Hello World!')
});

// use port 3000 unless there exists a preconfigured port
var port = process.env.port || 3000;

app.listen(port, function () {
    console.log('Example app listening on port ' + port)
});
