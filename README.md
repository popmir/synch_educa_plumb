> *Synchronization concepts should not be overshadowed by the implementation technology*

SynchEduca is a project created for teaching Java synchronization concepts, including [CyclicBarrier](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/CyclicBarrier.html), [CountDownLatch](https://download.java.net/java/early_access/valhalla/docs/api/java.base/java/util/concurrent/CountDownLatch.html), [Phaser](https://download.java.net/java/early_access/valhalla/docs/api/java.base/java/util/concurrent/Phaser.html), and [Semaphore](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/Semaphore.html). It provides a friendly try-and-play synchronization development environment and concurrent execution environment inside the browser. SynchEduca is created in continuation of the research described in [Application of social game context to teaching mutual exclusion](https://www.tandfonline.com/doi/pdf/10.1080/00051144.2018.1522462).

![Screenshot of SynchEduca UI](/docs/img/screenshot_latch.png "Screenshot of SynchEduca UI")

The development environment requires little or no knowledge of Java synchronization or the Java language at all. The creation of concurrent workflows is based on creating and connecting any number of widgets. Widgets are created in the large workspace area using the right sidebar interfaces and can be dragged to rearrange their position in the workspace area. The graphical connectivity of widgets is exposed through widget interface points and depends on the specific types of interfaces of created widgets. Only matching interfaces of two widgets can be connected.

Widgets are divided into two groups based on their role in workflow execution:
   - **Worker** widgets contain a list of statements and simply perform statements one after another (in an imperative way). Statements can be plain statements or any of the supported synchronization primitives that interact with the synchronization widgets. Synchronization primitives interact with the specific mechanism of the synchronization widget and, depending on the condition of the synchronization mechanism, can block the execution of the list of statements in the Worker widget.
   - **Synchronization** widgets provide a **graphical simplification of Java synchronization mechanisms**. Synchronization mechanisms contain no statements. Instead, they contain a state which is configured while creating the widget. The widget also contains interfaces (graphical ones) that allow for the change of the state based on a specific mechanism that is embodied into the widget.

# Requirements for usage

   - HTML5-compatible browser
   - Javascript enabled
   - Browser support tested for versions: Chrome version > 124, Firefox version > 125, Microsoft Edge version > 124

# Acknowledgements

Big thanks to the contributors of this project!

Attribution is hereby made to:
   - JSPlumb version 2.4.2 (nowadays there is a community edition [here](https://github.com/jsplumb/community-edition) of JSPlumb Toolkit [here](https://jsplumbtoolkit.com/))
   - jQuery 3.2.1
   - Bootstrap 4
   - doT version 1.1.1, a fast and concise JavaScript templating function with emphasis on performance ([GitHub link](https://github.com/olado/doT))

# Implementation notes (for the curious ones)

The project provides an IDE and a run-time engine. It is powered by vanilla Javascript and jQuery. It is a single-page application that is built using [MVP](https://www.geeksforgeeks.org/mvp-model-view-presenter-architecture-pattern-in-android-with-example/) (model-view-presenter) pattern. The initialization of dependencies of JSPlumb and SynchEduca components is invoked in `init.js`.

The run-time engine is implemented to provide a small kernel (since Javascript is a single thread running in a browser) which runs Worker widgets' statements with concurrency. The implementation is based on simplified concepts of [scheduler in operating system](https://www.geeksforgeeks.org/thread-states-in-operating-systems/). There is no waiting or delayed queue, just a ready queue, running queue, and blocked queues (assigned with each mechanism that can block execution).

Statement execution is just simulated with a timeout of 1 second. An [HTML5 Web Worker](https://developer.mozilla.org/en-US/docs/Web/API/Web_Workers_API/Using_web_workers) executes the timeout job of one second, after which it notifies back to the scheduler about the job completion. Notice that Worker can be extended to receive any job inside a message it receives. For a simulation of one second execution of a statement that does nothing, there is no need to execute simulation through Web Worker. But relying on Web Worker execution this way opens up the possibility for passing any Javascript job description in a message sent to the Web Worker. To get some insight into Web Worker use cases and limitations, one might look [here](https://web.dev/articles/off-main-thread), get familiar with [passing functions to Web Worker](https://medium.com/localazy/how-to-pass-function-to-web-workers-8acb485d6003) and structured clone algorithm limitations, and get inspiration from [this physics in Javascript example](https://dev.to/jerzakm/running-js-physics-in-a-webworker-part-1-proof-of-concept-ibj) or from known use cases of real-time updates to visualization of large data sets on a map, like [this large data sets on map example](https://blog.mapbox.com/clustering-millions-of-points-on-a-map-with-supercluster-272046ec5c97).

If you like this project, feel free to fork it.

The demo will be available at [http://brightlightness.com:3000/synch_educa/index.html](http://brightlightness.com:3000/synch_educa/index.html).

# Deployment

## Node.js local dev setup

Install Node.js > 20.x ([Node.js download link](https://nodejs.org/en))

1. Download this repo master branch
2. Position into the root folder
3. Run npm install
4. Run `PORT=3000 node app.js`
5. Go to `http://localhost:3000/synch_educa/index.html`

## Docker

There is a `Dockerfile` and a `build.sh` script to help create a Docker container.

After running build.sh, go to `http://YOURSERVER:3000/synch_educa/index.html`

# Continue where we left TODOs

We are not actively developing the project, but feel free to fork it. If you have any questions, feel free to contact us at [https://brightlightness.com](https://brightlightness.com), we will help if we can.

See what ongoing activities were started but are left not merged into the master branch. There are unmerged branches which implement authentication and integrating with LDAP server. The backend is written in Java Spring using Springboot. LDAP integration is implemented, and authentication was tested in the local development environment. Testing in the staging environment did not happen, but check if the code can be helpful if you plan to go that way.
